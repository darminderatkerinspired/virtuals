﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Main
struct Main_t2227614074;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>
struct List_1_t46448962;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745541_H
#define U3CMODULEU3E_T692745541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745541 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745541_H
#ifndef U3CTIMERU3EC__ITERATOR0_T685532230_H
#define U3CTIMERU3EC__ITERATOR0_T685532230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/<Timer>c__Iterator0
struct  U3CTimerU3Ec__Iterator0_t685532230  : public RuntimeObject
{
public:
	// System.Single Main/<Timer>c__Iterator0::t
	float ___t_0;
	// System.Single Main/<Timer>c__Iterator0::<rate>__0
	float ___U3CrateU3E__0_1;
	// System.Single Main/<Timer>c__Iterator0::<index>__0
	float ___U3CindexU3E__0_2;
	// System.Object Main/<Timer>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Main/<Timer>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Main/<Timer>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___t_0)); }
	inline float get_t_0() const { return ___t_0; }
	inline float* get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(float value)
	{
		___t_0 = value;
	}

	inline static int32_t get_offset_of_U3CrateU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U3CrateU3E__0_1)); }
	inline float get_U3CrateU3E__0_1() const { return ___U3CrateU3E__0_1; }
	inline float* get_address_of_U3CrateU3E__0_1() { return &___U3CrateU3E__0_1; }
	inline void set_U3CrateU3E__0_1(float value)
	{
		___U3CrateU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U3CindexU3E__0_2)); }
	inline float get_U3CindexU3E__0_2() const { return ___U3CindexU3E__0_2; }
	inline float* get_address_of_U3CindexU3E__0_2() { return &___U3CindexU3E__0_2; }
	inline void set_U3CindexU3E__0_2(float value)
	{
		___U3CindexU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERU3EC__ITERATOR0_T685532230_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SELECTION_T3531135257_H
#define SELECTION_T3531135257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/Selection
struct  Selection_t3531135257 
{
public:
	// System.Int32 Main/Selection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Selection_t3531135257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTION_T3531135257_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GUILABELS_T593977061_H
#define GUILABELS_T593977061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuiLabels
struct  GuiLabels_t593977061  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GuiLabels::change1
	float ___change1_3;
	// System.Single GuiLabels::change2
	float ___change2_4;
	// System.Single GuiLabels::change3
	float ___change3_5;
	// System.Single GuiLabels::change4
	float ___change4_6;
	// System.Single GuiLabels::change5
	float ___change5_7;
	// System.Int32 GuiLabels::change6
	int32_t ___change6_8;
	// Main GuiLabels::mainscript
	Main_t2227614074 * ___mainscript_9;
	// System.Collections.Generic.List`1<System.String> GuiLabels::betplacenames
	List_1_t3319525431 * ___betplacenames_10;

public:
	inline static int32_t get_offset_of_change1_3() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change1_3)); }
	inline float get_change1_3() const { return ___change1_3; }
	inline float* get_address_of_change1_3() { return &___change1_3; }
	inline void set_change1_3(float value)
	{
		___change1_3 = value;
	}

	inline static int32_t get_offset_of_change2_4() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change2_4)); }
	inline float get_change2_4() const { return ___change2_4; }
	inline float* get_address_of_change2_4() { return &___change2_4; }
	inline void set_change2_4(float value)
	{
		___change2_4 = value;
	}

	inline static int32_t get_offset_of_change3_5() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change3_5)); }
	inline float get_change3_5() const { return ___change3_5; }
	inline float* get_address_of_change3_5() { return &___change3_5; }
	inline void set_change3_5(float value)
	{
		___change3_5 = value;
	}

	inline static int32_t get_offset_of_change4_6() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change4_6)); }
	inline float get_change4_6() const { return ___change4_6; }
	inline float* get_address_of_change4_6() { return &___change4_6; }
	inline void set_change4_6(float value)
	{
		___change4_6 = value;
	}

	inline static int32_t get_offset_of_change5_7() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change5_7)); }
	inline float get_change5_7() const { return ___change5_7; }
	inline float* get_address_of_change5_7() { return &___change5_7; }
	inline void set_change5_7(float value)
	{
		___change5_7 = value;
	}

	inline static int32_t get_offset_of_change6_8() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change6_8)); }
	inline int32_t get_change6_8() const { return ___change6_8; }
	inline int32_t* get_address_of_change6_8() { return &___change6_8; }
	inline void set_change6_8(int32_t value)
	{
		___change6_8 = value;
	}

	inline static int32_t get_offset_of_mainscript_9() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___mainscript_9)); }
	inline Main_t2227614074 * get_mainscript_9() const { return ___mainscript_9; }
	inline Main_t2227614074 ** get_address_of_mainscript_9() { return &___mainscript_9; }
	inline void set_mainscript_9(Main_t2227614074 * value)
	{
		___mainscript_9 = value;
		Il2CppCodeGenWriteBarrier((&___mainscript_9), value);
	}

	inline static int32_t get_offset_of_betplacenames_10() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___betplacenames_10)); }
	inline List_1_t3319525431 * get_betplacenames_10() const { return ___betplacenames_10; }
	inline List_1_t3319525431 ** get_address_of_betplacenames_10() { return &___betplacenames_10; }
	inline void set_betplacenames_10(List_1_t3319525431 * value)
	{
		___betplacenames_10 = value;
		Il2CppCodeGenWriteBarrier((&___betplacenames_10), value);
	}
};

struct GuiLabels_t593977061_StaticFields
{
public:
	// System.Int32 GuiLabels::GUIdepth
	int32_t ___GUIdepth_2;

public:
	inline static int32_t get_offset_of_GUIdepth_2() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061_StaticFields, ___GUIdepth_2)); }
	inline int32_t get_GUIdepth_2() const { return ___GUIdepth_2; }
	inline int32_t* get_address_of_GUIdepth_2() { return &___GUIdepth_2; }
	inline void set_GUIdepth_2(int32_t value)
	{
		___GUIdepth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILABELS_T593977061_H
#ifndef MAIN_T2227614074_H
#define MAIN_T2227614074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2227614074  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Main::shade_alpha
	float ___shade_alpha_2;
	// System.Single Main::change1
	float ___change1_5;
	// System.Single Main::change2
	float ___change2_6;
	// System.Single Main::change3
	float ___change3_7;
	// System.Single Main::change4
	float ___change4_8;
	// System.Single Main::change5
	float ___change5_9;
	// System.Int32 Main::change6
	int32_t ___change6_10;
	// System.Boolean Main::click_once
	bool ___click_once_11;
	// System.Boolean Main::chip_notpressed
	bool ___chip_notpressed_12;
	// UnityEngine.Texture2D Main::Template
	Texture2D_t3840446185 * ___Template_13;
	// UnityEngine.Texture2D Main::shade
	Texture2D_t3840446185 * ___shade_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Main::sceneobjects
	List_1_t2585711361 * ___sceneobjects_15;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::horseracers_images
	List_1_t1017553631 * ___horseracers_images_16;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::bet_btns
	List_1_t1017553631 * ___bet_btns_17;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::chips_images
	List_1_t1017553631 * ___chips_images_18;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_win
	List_1_t2869341516 * ___horse_bets_win_19;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_place
	List_1_t2869341516 * ___horse_bets_place_20;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_show
	List_1_t2869341516 * ___horse_bets_show_21;
	// System.Collections.Generic.List`1<System.Int32> Main::horses_finish
	List_1_t128053199 * ___horses_finish_22;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_win
	List_1_t2869341516 * ___current_bets_win_23;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_place
	List_1_t2869341516 * ___current_bets_place_24;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_show
	List_1_t2869341516 * ___current_bets_show_25;
	// System.Collections.Generic.List`1<System.String> Main::horseracers_names
	List_1_t3319525431 * ___horseracers_names_26;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>> Main::bet_odds_master
	List_1_t46448962 * ___bet_odds_master_27;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>> Main::current_bets_master
	List_1_t46448962 * ___current_bets_master_28;
	// Main/Selection Main::currentSel
	int32_t ___currentSel_29;
	// System.Single Main::stake
	float ___stake_30;
	// System.Single Main::visual_timer
	float ___visual_timer_31;
	// System.Int32 Main::current_bet_type
	int32_t ___current_bet_type_32;

public:
	inline static int32_t get_offset_of_shade_alpha_2() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___shade_alpha_2)); }
	inline float get_shade_alpha_2() const { return ___shade_alpha_2; }
	inline float* get_address_of_shade_alpha_2() { return &___shade_alpha_2; }
	inline void set_shade_alpha_2(float value)
	{
		___shade_alpha_2 = value;
	}

	inline static int32_t get_offset_of_change1_5() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change1_5)); }
	inline float get_change1_5() const { return ___change1_5; }
	inline float* get_address_of_change1_5() { return &___change1_5; }
	inline void set_change1_5(float value)
	{
		___change1_5 = value;
	}

	inline static int32_t get_offset_of_change2_6() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change2_6)); }
	inline float get_change2_6() const { return ___change2_6; }
	inline float* get_address_of_change2_6() { return &___change2_6; }
	inline void set_change2_6(float value)
	{
		___change2_6 = value;
	}

	inline static int32_t get_offset_of_change3_7() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change3_7)); }
	inline float get_change3_7() const { return ___change3_7; }
	inline float* get_address_of_change3_7() { return &___change3_7; }
	inline void set_change3_7(float value)
	{
		___change3_7 = value;
	}

	inline static int32_t get_offset_of_change4_8() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change4_8)); }
	inline float get_change4_8() const { return ___change4_8; }
	inline float* get_address_of_change4_8() { return &___change4_8; }
	inline void set_change4_8(float value)
	{
		___change4_8 = value;
	}

	inline static int32_t get_offset_of_change5_9() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change5_9)); }
	inline float get_change5_9() const { return ___change5_9; }
	inline float* get_address_of_change5_9() { return &___change5_9; }
	inline void set_change5_9(float value)
	{
		___change5_9 = value;
	}

	inline static int32_t get_offset_of_change6_10() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change6_10)); }
	inline int32_t get_change6_10() const { return ___change6_10; }
	inline int32_t* get_address_of_change6_10() { return &___change6_10; }
	inline void set_change6_10(int32_t value)
	{
		___change6_10 = value;
	}

	inline static int32_t get_offset_of_click_once_11() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___click_once_11)); }
	inline bool get_click_once_11() const { return ___click_once_11; }
	inline bool* get_address_of_click_once_11() { return &___click_once_11; }
	inline void set_click_once_11(bool value)
	{
		___click_once_11 = value;
	}

	inline static int32_t get_offset_of_chip_notpressed_12() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___chip_notpressed_12)); }
	inline bool get_chip_notpressed_12() const { return ___chip_notpressed_12; }
	inline bool* get_address_of_chip_notpressed_12() { return &___chip_notpressed_12; }
	inline void set_chip_notpressed_12(bool value)
	{
		___chip_notpressed_12 = value;
	}

	inline static int32_t get_offset_of_Template_13() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___Template_13)); }
	inline Texture2D_t3840446185 * get_Template_13() const { return ___Template_13; }
	inline Texture2D_t3840446185 ** get_address_of_Template_13() { return &___Template_13; }
	inline void set_Template_13(Texture2D_t3840446185 * value)
	{
		___Template_13 = value;
		Il2CppCodeGenWriteBarrier((&___Template_13), value);
	}

	inline static int32_t get_offset_of_shade_14() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___shade_14)); }
	inline Texture2D_t3840446185 * get_shade_14() const { return ___shade_14; }
	inline Texture2D_t3840446185 ** get_address_of_shade_14() { return &___shade_14; }
	inline void set_shade_14(Texture2D_t3840446185 * value)
	{
		___shade_14 = value;
		Il2CppCodeGenWriteBarrier((&___shade_14), value);
	}

	inline static int32_t get_offset_of_sceneobjects_15() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___sceneobjects_15)); }
	inline List_1_t2585711361 * get_sceneobjects_15() const { return ___sceneobjects_15; }
	inline List_1_t2585711361 ** get_address_of_sceneobjects_15() { return &___sceneobjects_15; }
	inline void set_sceneobjects_15(List_1_t2585711361 * value)
	{
		___sceneobjects_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneobjects_15), value);
	}

	inline static int32_t get_offset_of_horseracers_images_16() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horseracers_images_16)); }
	inline List_1_t1017553631 * get_horseracers_images_16() const { return ___horseracers_images_16; }
	inline List_1_t1017553631 ** get_address_of_horseracers_images_16() { return &___horseracers_images_16; }
	inline void set_horseracers_images_16(List_1_t1017553631 * value)
	{
		___horseracers_images_16 = value;
		Il2CppCodeGenWriteBarrier((&___horseracers_images_16), value);
	}

	inline static int32_t get_offset_of_bet_btns_17() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___bet_btns_17)); }
	inline List_1_t1017553631 * get_bet_btns_17() const { return ___bet_btns_17; }
	inline List_1_t1017553631 ** get_address_of_bet_btns_17() { return &___bet_btns_17; }
	inline void set_bet_btns_17(List_1_t1017553631 * value)
	{
		___bet_btns_17 = value;
		Il2CppCodeGenWriteBarrier((&___bet_btns_17), value);
	}

	inline static int32_t get_offset_of_chips_images_18() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___chips_images_18)); }
	inline List_1_t1017553631 * get_chips_images_18() const { return ___chips_images_18; }
	inline List_1_t1017553631 ** get_address_of_chips_images_18() { return &___chips_images_18; }
	inline void set_chips_images_18(List_1_t1017553631 * value)
	{
		___chips_images_18 = value;
		Il2CppCodeGenWriteBarrier((&___chips_images_18), value);
	}

	inline static int32_t get_offset_of_horse_bets_win_19() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_win_19)); }
	inline List_1_t2869341516 * get_horse_bets_win_19() const { return ___horse_bets_win_19; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_win_19() { return &___horse_bets_win_19; }
	inline void set_horse_bets_win_19(List_1_t2869341516 * value)
	{
		___horse_bets_win_19 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_win_19), value);
	}

	inline static int32_t get_offset_of_horse_bets_place_20() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_place_20)); }
	inline List_1_t2869341516 * get_horse_bets_place_20() const { return ___horse_bets_place_20; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_place_20() { return &___horse_bets_place_20; }
	inline void set_horse_bets_place_20(List_1_t2869341516 * value)
	{
		___horse_bets_place_20 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_place_20), value);
	}

	inline static int32_t get_offset_of_horse_bets_show_21() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_show_21)); }
	inline List_1_t2869341516 * get_horse_bets_show_21() const { return ___horse_bets_show_21; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_show_21() { return &___horse_bets_show_21; }
	inline void set_horse_bets_show_21(List_1_t2869341516 * value)
	{
		___horse_bets_show_21 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_show_21), value);
	}

	inline static int32_t get_offset_of_horses_finish_22() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horses_finish_22)); }
	inline List_1_t128053199 * get_horses_finish_22() const { return ___horses_finish_22; }
	inline List_1_t128053199 ** get_address_of_horses_finish_22() { return &___horses_finish_22; }
	inline void set_horses_finish_22(List_1_t128053199 * value)
	{
		___horses_finish_22 = value;
		Il2CppCodeGenWriteBarrier((&___horses_finish_22), value);
	}

	inline static int32_t get_offset_of_current_bets_win_23() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_win_23)); }
	inline List_1_t2869341516 * get_current_bets_win_23() const { return ___current_bets_win_23; }
	inline List_1_t2869341516 ** get_address_of_current_bets_win_23() { return &___current_bets_win_23; }
	inline void set_current_bets_win_23(List_1_t2869341516 * value)
	{
		___current_bets_win_23 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_win_23), value);
	}

	inline static int32_t get_offset_of_current_bets_place_24() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_place_24)); }
	inline List_1_t2869341516 * get_current_bets_place_24() const { return ___current_bets_place_24; }
	inline List_1_t2869341516 ** get_address_of_current_bets_place_24() { return &___current_bets_place_24; }
	inline void set_current_bets_place_24(List_1_t2869341516 * value)
	{
		___current_bets_place_24 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_place_24), value);
	}

	inline static int32_t get_offset_of_current_bets_show_25() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_show_25)); }
	inline List_1_t2869341516 * get_current_bets_show_25() const { return ___current_bets_show_25; }
	inline List_1_t2869341516 ** get_address_of_current_bets_show_25() { return &___current_bets_show_25; }
	inline void set_current_bets_show_25(List_1_t2869341516 * value)
	{
		___current_bets_show_25 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_show_25), value);
	}

	inline static int32_t get_offset_of_horseracers_names_26() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horseracers_names_26)); }
	inline List_1_t3319525431 * get_horseracers_names_26() const { return ___horseracers_names_26; }
	inline List_1_t3319525431 ** get_address_of_horseracers_names_26() { return &___horseracers_names_26; }
	inline void set_horseracers_names_26(List_1_t3319525431 * value)
	{
		___horseracers_names_26 = value;
		Il2CppCodeGenWriteBarrier((&___horseracers_names_26), value);
	}

	inline static int32_t get_offset_of_bet_odds_master_27() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___bet_odds_master_27)); }
	inline List_1_t46448962 * get_bet_odds_master_27() const { return ___bet_odds_master_27; }
	inline List_1_t46448962 ** get_address_of_bet_odds_master_27() { return &___bet_odds_master_27; }
	inline void set_bet_odds_master_27(List_1_t46448962 * value)
	{
		___bet_odds_master_27 = value;
		Il2CppCodeGenWriteBarrier((&___bet_odds_master_27), value);
	}

	inline static int32_t get_offset_of_current_bets_master_28() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_master_28)); }
	inline List_1_t46448962 * get_current_bets_master_28() const { return ___current_bets_master_28; }
	inline List_1_t46448962 ** get_address_of_current_bets_master_28() { return &___current_bets_master_28; }
	inline void set_current_bets_master_28(List_1_t46448962 * value)
	{
		___current_bets_master_28 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_master_28), value);
	}

	inline static int32_t get_offset_of_currentSel_29() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___currentSel_29)); }
	inline int32_t get_currentSel_29() const { return ___currentSel_29; }
	inline int32_t* get_address_of_currentSel_29() { return &___currentSel_29; }
	inline void set_currentSel_29(int32_t value)
	{
		___currentSel_29 = value;
	}

	inline static int32_t get_offset_of_stake_30() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___stake_30)); }
	inline float get_stake_30() const { return ___stake_30; }
	inline float* get_address_of_stake_30() { return &___stake_30; }
	inline void set_stake_30(float value)
	{
		___stake_30 = value;
	}

	inline static int32_t get_offset_of_visual_timer_31() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___visual_timer_31)); }
	inline float get_visual_timer_31() const { return ___visual_timer_31; }
	inline float* get_address_of_visual_timer_31() { return &___visual_timer_31; }
	inline void set_visual_timer_31(float value)
	{
		___visual_timer_31 = value;
	}

	inline static int32_t get_offset_of_current_bet_type_32() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bet_type_32)); }
	inline int32_t get_current_bet_type_32() const { return ___current_bet_type_32; }
	inline int32_t* get_address_of_current_bet_type_32() { return &___current_bet_type_32; }
	inline void set_current_bet_type_32(int32_t value)
	{
		___current_bet_type_32 = value;
	}
};

struct Main_t2227614074_StaticFields
{
public:
	// System.Int32 Main::chip_gui_depth
	int32_t ___chip_gui_depth_3;
	// System.Int32 Main::gui_depth
	int32_t ___gui_depth_4;

public:
	inline static int32_t get_offset_of_chip_gui_depth_3() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___chip_gui_depth_3)); }
	inline int32_t get_chip_gui_depth_3() const { return ___chip_gui_depth_3; }
	inline int32_t* get_address_of_chip_gui_depth_3() { return &___chip_gui_depth_3; }
	inline void set_chip_gui_depth_3(int32_t value)
	{
		___chip_gui_depth_3 = value;
	}

	inline static int32_t get_offset_of_gui_depth_4() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___gui_depth_4)); }
	inline int32_t get_gui_depth_4() const { return ___gui_depth_4; }
	inline int32_t* get_address_of_gui_depth_4() { return &___gui_depth_4; }
	inline void set_gui_depth_4(int32_t value)
	{
		___gui_depth_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T2227614074_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1500[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (U3CModuleU3E_t692745541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (GuiLabels_t593977061), -1, sizeof(GuiLabels_t593977061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1506[9] = 
{
	GuiLabels_t593977061_StaticFields::get_offset_of_GUIdepth_2(),
	GuiLabels_t593977061::get_offset_of_change1_3(),
	GuiLabels_t593977061::get_offset_of_change2_4(),
	GuiLabels_t593977061::get_offset_of_change3_5(),
	GuiLabels_t593977061::get_offset_of_change4_6(),
	GuiLabels_t593977061::get_offset_of_change5_7(),
	GuiLabels_t593977061::get_offset_of_change6_8(),
	GuiLabels_t593977061::get_offset_of_mainscript_9(),
	GuiLabels_t593977061::get_offset_of_betplacenames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (Main_t2227614074), -1, sizeof(Main_t2227614074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1507[31] = 
{
	Main_t2227614074::get_offset_of_shade_alpha_2(),
	Main_t2227614074_StaticFields::get_offset_of_chip_gui_depth_3(),
	Main_t2227614074_StaticFields::get_offset_of_gui_depth_4(),
	Main_t2227614074::get_offset_of_change1_5(),
	Main_t2227614074::get_offset_of_change2_6(),
	Main_t2227614074::get_offset_of_change3_7(),
	Main_t2227614074::get_offset_of_change4_8(),
	Main_t2227614074::get_offset_of_change5_9(),
	Main_t2227614074::get_offset_of_change6_10(),
	Main_t2227614074::get_offset_of_click_once_11(),
	Main_t2227614074::get_offset_of_chip_notpressed_12(),
	Main_t2227614074::get_offset_of_Template_13(),
	Main_t2227614074::get_offset_of_shade_14(),
	Main_t2227614074::get_offset_of_sceneobjects_15(),
	Main_t2227614074::get_offset_of_horseracers_images_16(),
	Main_t2227614074::get_offset_of_bet_btns_17(),
	Main_t2227614074::get_offset_of_chips_images_18(),
	Main_t2227614074::get_offset_of_horse_bets_win_19(),
	Main_t2227614074::get_offset_of_horse_bets_place_20(),
	Main_t2227614074::get_offset_of_horse_bets_show_21(),
	Main_t2227614074::get_offset_of_horses_finish_22(),
	Main_t2227614074::get_offset_of_current_bets_win_23(),
	Main_t2227614074::get_offset_of_current_bets_place_24(),
	Main_t2227614074::get_offset_of_current_bets_show_25(),
	Main_t2227614074::get_offset_of_horseracers_names_26(),
	Main_t2227614074::get_offset_of_bet_odds_master_27(),
	Main_t2227614074::get_offset_of_current_bets_master_28(),
	Main_t2227614074::get_offset_of_currentSel_29(),
	Main_t2227614074::get_offset_of_stake_30(),
	Main_t2227614074::get_offset_of_visual_timer_31(),
	Main_t2227614074::get_offset_of_current_bet_type_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (Selection_t3531135257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1508[4] = 
{
	Selection_t3531135257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (U3CTimerU3Ec__Iterator0_t685532230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[6] = 
{
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_t_0(),
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_U3CrateU3E__0_1(),
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_U3CindexU3E__0_2(),
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_U24current_3(),
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_U24disposing_4(),
	U3CTimerU3Ec__Iterator0_t685532230::get_offset_of_U24PC_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
