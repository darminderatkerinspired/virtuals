﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GuiLabels
struct GuiLabels_t593977061;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>
struct List_1_t46448962;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture
struct Texture_t3661962703;
// Main
struct Main_t2227614074;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// Main/<Timer>c__Iterator0
struct U3CTimerU3Ec__Iterator0_t685532230;
// UnityEngine.Event
struct Event_t2956885303;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// System.Collections.Generic.List`1<System.Single>[]
struct List_1U5BU5D_t345528325;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;

extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const uint32_t GuiLabels__ctor_m3778019952_MetadataUsageId;
extern RuntimeClass* Main_t2227614074_il2cpp_TypeInfo_var;
extern const uint32_t GuiLabels_Start_m3203687160_MetadataUsageId;
extern const uint32_t GuiLabels_Gui_Change_m3083760333_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m2670531749_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m718437397_RuntimeMethod_var;
extern String_t* _stringLiteral3452614621;
extern const uint32_t GuiLabels_bet_Value_topdisplay_m2233024845_MetadataUsageId;
extern RuntimeClass* GuiLabels_t593977061_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m1676150876_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2840606808_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3379783094_RuntimeMethod_var;
extern const uint32_t GuiLabels_OnGUI_m1879780399_MetadataUsageId;
extern RuntimeClass* List_1_t2585711361_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1017553631_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2869341516_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t128053199_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t46448962_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3526297712_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m90174447_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3152364487_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1628857705_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1412855716_RuntimeMethod_var;
extern const uint32_t Main__ctor_m136889595_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m3328803046_RuntimeMethod_var;
extern const uint32_t Main_Start_m1779287939_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m2269680114_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m697420525_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m4125879936_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m186164705_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m888956288_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m2770200702_RuntimeMethod_var;
extern const uint32_t Main_randomwinners_m793992468_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m774866856_RuntimeMethod_var;
extern String_t* _stringLiteral984239891;
extern String_t* _stringLiteral1111741867;
extern String_t* _stringLiteral1863184053;
extern String_t* _stringLiteral2028776514;
extern const uint32_t Main_Calculate_wins_m1407021724_MetadataUsageId;
extern RuntimeClass* U3CTimerU3Ec__Iterator0_t685532230_il2cpp_TypeInfo_var;
extern const uint32_t Main_Timer_m2167688892_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3978655417_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2024781851_RuntimeMethod_var;
extern const uint32_t Main_reset_bets_m1112432460_MetadataUsageId;
extern const RuntimeMethod* List_1_set_Item_m722690934_RuntimeMethod_var;
extern const uint32_t Main_updatebet_m529302127_MetadataUsageId;
extern const uint32_t Main_typeofbutton_m2032441888_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t3956901511_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1130393540;
extern String_t* _stringLiteral1945711025;
extern String_t* _stringLiteral2474120682;
extern String_t* _stringLiteral326071421;
extern String_t* _stringLiteral4122535824;
extern const uint32_t Main_OnGUI_m1948052770_MetadataUsageId;
extern const uint32_t Main__cctor_m2675706324_MetadataUsageId;
extern String_t* _stringLiteral2791739664;
extern const uint32_t U3CTimerU3Ec__Iterator0_MoveNext_m3832737386_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerU3Ec__Iterator0_Reset_m2192134946_MetadataUsageId;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct GUIStyleState_t1397964415_marshaled_com;
struct RectOffset_t1369453676_marshaled_com;

struct ObjectU5BU5D_t2843939325;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745541_H
#define U3CMODULEU3E_T692745541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745541 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745541_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef LIST_1_T1017553631_H
#define LIST_1_T1017553631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct  List_1_t1017553631  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Texture2DU5BU5D_t149664596* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1017553631, ____items_1)); }
	inline Texture2DU5BU5D_t149664596* get__items_1() const { return ____items_1; }
	inline Texture2DU5BU5D_t149664596** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Texture2DU5BU5D_t149664596* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1017553631, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1017553631, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1017553631_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Texture2DU5BU5D_t149664596* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1017553631_StaticFields, ___EmptyArray_4)); }
	inline Texture2DU5BU5D_t149664596* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Texture2DU5BU5D_t149664596** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Texture2DU5BU5D_t149664596* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1017553631_H
#ifndef LIST_1_T46448962_H
#define LIST_1_T46448962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>
struct  List_1_t46448962  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_t345528325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t46448962, ____items_1)); }
	inline List_1U5BU5D_t345528325* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_t345528325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_t345528325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t46448962, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t46448962, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t46448962_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	List_1U5BU5D_t345528325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t46448962_StaticFields, ___EmptyArray_4)); }
	inline List_1U5BU5D_t345528325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline List_1U5BU5D_t345528325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(List_1U5BU5D_t345528325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T46448962_H
#ifndef LIST_1_T2869341516_H
#define LIST_1_T2869341516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Single>
struct  List_1_t2869341516  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SingleU5BU5D_t1444911251* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____items_1)); }
	inline SingleU5BU5D_t1444911251* get__items_1() const { return ____items_1; }
	inline SingleU5BU5D_t1444911251** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SingleU5BU5D_t1444911251* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2869341516, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2869341516_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	SingleU5BU5D_t1444911251* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2869341516_StaticFields, ___EmptyArray_4)); }
	inline SingleU5BU5D_t1444911251* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline SingleU5BU5D_t1444911251** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(SingleU5BU5D_t1444911251* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2869341516_H
#ifndef U3CTIMERU3EC__ITERATOR0_T685532230_H
#define U3CTIMERU3EC__ITERATOR0_T685532230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/<Timer>c__Iterator0
struct  U3CTimerU3Ec__Iterator0_t685532230  : public RuntimeObject
{
public:
	// System.Single Main/<Timer>c__Iterator0::t
	float ___t_0;
	// System.Single Main/<Timer>c__Iterator0::<rate>__0
	float ___U3CrateU3E__0_1;
	// System.Single Main/<Timer>c__Iterator0::<index>__0
	float ___U3CindexU3E__0_2;
	// System.Object Main/<Timer>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Main/<Timer>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Main/<Timer>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___t_0)); }
	inline float get_t_0() const { return ___t_0; }
	inline float* get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(float value)
	{
		___t_0 = value;
	}

	inline static int32_t get_offset_of_U3CrateU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U3CrateU3E__0_1)); }
	inline float get_U3CrateU3E__0_1() const { return ___U3CrateU3E__0_1; }
	inline float* get_address_of_U3CrateU3E__0_1() { return &___U3CrateU3E__0_1; }
	inline void set_U3CrateU3E__0_1(float value)
	{
		___U3CrateU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U3CindexU3E__0_2)); }
	inline float get_U3CindexU3E__0_2() const { return ___U3CindexU3E__0_2; }
	inline float* get_address_of_U3CindexU3E__0_2() { return &___U3CindexU3E__0_2; }
	inline void set_U3CindexU3E__0_2(float value)
	{
		___U3CindexU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTimerU3Ec__Iterator0_t685532230, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERU3EC__ITERATOR0_T685532230_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LIST_1_T2585711361_H
#define LIST_1_T2585711361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2585711361  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3328599146* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____items_1)); }
	inline GameObjectU5BU5D_t3328599146* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3328599146* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2585711361_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3328599146* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2585711361_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3328599146* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3328599146* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2585711361_H
#ifndef LIST_1_T128053199_H
#define LIST_1_T128053199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t128053199  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t385246372* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____items_1)); }
	inline Int32U5BU5D_t385246372* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t385246372** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t385246372* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t128053199_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t385246372* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t128053199_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t385246372* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t385246372* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T128053199_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef SELECTION_T3531135257_H
#define SELECTION_T3531135257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/Selection
struct  Selection_t3531135257 
{
public:
	// System.Int32 Main/Selection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Selection_t3531135257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTION_T3531135257_H
#ifndef EVENT_T2956885303_H
#define EVENT_T2956885303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Event
struct  Event_t2956885303  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Event_t2956885303, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

struct Event_t2956885303_StaticFields
{
public:
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_t2956885303 * ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_t2956885303 * ___s_MasterEvent_2;

public:
	inline static int32_t get_offset_of_s_Current_1() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_Current_1)); }
	inline Event_t2956885303 * get_s_Current_1() const { return ___s_Current_1; }
	inline Event_t2956885303 ** get_address_of_s_Current_1() { return &___s_Current_1; }
	inline void set_s_Current_1(Event_t2956885303 * value)
	{
		___s_Current_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Current_1), value);
	}

	inline static int32_t get_offset_of_s_MasterEvent_2() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_MasterEvent_2)); }
	inline Event_t2956885303 * get_s_MasterEvent_2() const { return ___s_MasterEvent_2; }
	inline Event_t2956885303 ** get_address_of_s_MasterEvent_2() { return &___s_MasterEvent_2; }
	inline void set_s_MasterEvent_2(Event_t2956885303 * value)
	{
		___s_MasterEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_MasterEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // EVENT_T2956885303_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;
	// UnityEngine.Font UnityEngine.GUIStyle::m_FontInternal
	Font_t1956802104 * ___m_FontInternal_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}

	inline static int32_t get_offset_of_m_FontInternal_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_FontInternal_13)); }
	inline Font_t1956802104 * get_m_FontInternal_13() const { return ___m_FontInternal_13; }
	inline Font_t1956802104 ** get_address_of_m_FontInternal_13() { return &___m_FontInternal_13; }
	inline void set_m_FontInternal_13(Font_t1956802104 * value)
	{
		___m_FontInternal_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontInternal_13), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_15)); }
	inline GUIStyle_t3956901511 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t3956901511 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
#endif // GUISTYLE_T3956901511_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_2;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_3;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_22;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_23;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_24;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_25;

public:
	inline static int32_t get_offset_of_m_Font_2() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_2)); }
	inline Font_t1956802104 * get_m_Font_2() const { return ___m_Font_2; }
	inline Font_t1956802104 ** get_address_of_m_Font_2() { return &___m_Font_2; }
	inline void set_m_Font_2(Font_t1956802104 * value)
	{
		___m_Font_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_2), value);
	}

	inline static int32_t get_offset_of_m_box_3() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_3)); }
	inline GUIStyle_t3956901511 * get_m_box_3() const { return ___m_box_3; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_3() { return &___m_box_3; }
	inline void set_m_box_3(GUIStyle_t3956901511 * value)
	{
		___m_box_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_3), value);
	}

	inline static int32_t get_offset_of_m_button_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_4)); }
	inline GUIStyle_t3956901511 * get_m_button_4() const { return ___m_button_4; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_4() { return &___m_button_4; }
	inline void set_m_button_4(GUIStyle_t3956901511 * value)
	{
		___m_button_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_4), value);
	}

	inline static int32_t get_offset_of_m_toggle_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_5)); }
	inline GUIStyle_t3956901511 * get_m_toggle_5() const { return ___m_toggle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_5() { return &___m_toggle_5; }
	inline void set_m_toggle_5(GUIStyle_t3956901511 * value)
	{
		___m_toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_5), value);
	}

	inline static int32_t get_offset_of_m_label_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_6)); }
	inline GUIStyle_t3956901511 * get_m_label_6() const { return ___m_label_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_6() { return &___m_label_6; }
	inline void set_m_label_6(GUIStyle_t3956901511 * value)
	{
		___m_label_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_6), value);
	}

	inline static int32_t get_offset_of_m_textField_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_7)); }
	inline GUIStyle_t3956901511 * get_m_textField_7() const { return ___m_textField_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_7() { return &___m_textField_7; }
	inline void set_m_textField_7(GUIStyle_t3956901511 * value)
	{
		___m_textField_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_7), value);
	}

	inline static int32_t get_offset_of_m_textArea_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_8)); }
	inline GUIStyle_t3956901511 * get_m_textArea_8() const { return ___m_textArea_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_8() { return &___m_textArea_8; }
	inline void set_m_textArea_8(GUIStyle_t3956901511 * value)
	{
		___m_textArea_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_8), value);
	}

	inline static int32_t get_offset_of_m_window_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_9)); }
	inline GUIStyle_t3956901511 * get_m_window_9() const { return ___m_window_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_9() { return &___m_window_9; }
	inline void set_m_window_9(GUIStyle_t3956901511 * value)
	{
		___m_window_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_9), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_10)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_10() const { return ___m_horizontalSlider_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_10() { return &___m_horizontalSlider_10; }
	inline void set_m_horizontalSlider_10(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_10), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_11)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_11() const { return ___m_horizontalSliderThumb_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_11() { return &___m_horizontalSliderThumb_11; }
	inline void set_m_horizontalSliderThumb_11(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_11), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_12() const { return ___m_verticalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_12() { return &___m_verticalSlider_12; }
	inline void set_m_verticalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_13() const { return ___m_verticalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_13() { return &___m_verticalSliderThumb_13; }
	inline void set_m_verticalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_14)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_14() const { return ___m_horizontalScrollbar_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_14() { return &___m_horizontalScrollbar_14; }
	inline void set_m_horizontalScrollbar_14(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_15() const { return ___m_horizontalScrollbarThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_15() { return &___m_horizontalScrollbarThumb_15; }
	inline void set_m_horizontalScrollbarThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_16() const { return ___m_horizontalScrollbarLeftButton_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_16() { return &___m_horizontalScrollbarLeftButton_16; }
	inline void set_m_horizontalScrollbarLeftButton_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_17() const { return ___m_horizontalScrollbarRightButton_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_17() { return &___m_horizontalScrollbarRightButton_17; }
	inline void set_m_horizontalScrollbarRightButton_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_17), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_18)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_18() const { return ___m_verticalScrollbar_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_18() { return &___m_verticalScrollbar_18; }
	inline void set_m_verticalScrollbar_18(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_18), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_19)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_19() const { return ___m_verticalScrollbarThumb_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_19() { return &___m_verticalScrollbarThumb_19; }
	inline void set_m_verticalScrollbarThumb_19(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_20() const { return ___m_verticalScrollbarUpButton_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_20() { return &___m_verticalScrollbarUpButton_20; }
	inline void set_m_verticalScrollbarUpButton_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_21() const { return ___m_verticalScrollbarDownButton_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_21() { return &___m_verticalScrollbarDownButton_21; }
	inline void set_m_verticalScrollbarDownButton_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_21), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_22)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_22() const { return ___m_ScrollView_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_22() { return &___m_ScrollView_22; }
	inline void set_m_ScrollView_22(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_22), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_23)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_23() const { return ___m_CustomStyles_23; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_23() { return &___m_CustomStyles_23; }
	inline void set_m_CustomStyles_23(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_23), value);
	}

	inline static int32_t get_offset_of_m_Settings_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_24)); }
	inline GUISettings_t1774757634 * get_m_Settings_24() const { return ___m_Settings_24; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_24() { return &___m_Settings_24; }
	inline void set_m_Settings_24(GUISettings_t1774757634 * value)
	{
		___m_Settings_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_24), value);
	}

	inline static int32_t get_offset_of_m_Styles_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_25)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_25() const { return ___m_Styles_25; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_25() { return &___m_Styles_25; }
	inline void set_m_Styles_25(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_25), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_26;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_27;

public:
	inline static int32_t get_offset_of_m_SkinChanged_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_26)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_26() const { return ___m_SkinChanged_26; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_26() { return &___m_SkinChanged_26; }
	inline void set_m_SkinChanged_26(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_26), value);
	}

	inline static int32_t get_offset_of_current_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_27)); }
	inline GUISkin_t1244372282 * get_current_27() const { return ___current_27; }
	inline GUISkin_t1244372282 ** get_address_of_current_27() { return &___current_27; }
	inline void set_current_27(GUISkin_t1244372282 * value)
	{
		___current_27 = value;
		Il2CppCodeGenWriteBarrier((&___current_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GUILABELS_T593977061_H
#define GUILABELS_T593977061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuiLabels
struct  GuiLabels_t593977061  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GuiLabels::change1
	float ___change1_3;
	// System.Single GuiLabels::change2
	float ___change2_4;
	// System.Single GuiLabels::change3
	float ___change3_5;
	// System.Single GuiLabels::change4
	float ___change4_6;
	// System.Single GuiLabels::change5
	float ___change5_7;
	// System.Int32 GuiLabels::change6
	int32_t ___change6_8;
	// Main GuiLabels::mainscript
	Main_t2227614074 * ___mainscript_9;
	// System.Collections.Generic.List`1<System.String> GuiLabels::betplacenames
	List_1_t3319525431 * ___betplacenames_10;

public:
	inline static int32_t get_offset_of_change1_3() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change1_3)); }
	inline float get_change1_3() const { return ___change1_3; }
	inline float* get_address_of_change1_3() { return &___change1_3; }
	inline void set_change1_3(float value)
	{
		___change1_3 = value;
	}

	inline static int32_t get_offset_of_change2_4() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change2_4)); }
	inline float get_change2_4() const { return ___change2_4; }
	inline float* get_address_of_change2_4() { return &___change2_4; }
	inline void set_change2_4(float value)
	{
		___change2_4 = value;
	}

	inline static int32_t get_offset_of_change3_5() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change3_5)); }
	inline float get_change3_5() const { return ___change3_5; }
	inline float* get_address_of_change3_5() { return &___change3_5; }
	inline void set_change3_5(float value)
	{
		___change3_5 = value;
	}

	inline static int32_t get_offset_of_change4_6() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change4_6)); }
	inline float get_change4_6() const { return ___change4_6; }
	inline float* get_address_of_change4_6() { return &___change4_6; }
	inline void set_change4_6(float value)
	{
		___change4_6 = value;
	}

	inline static int32_t get_offset_of_change5_7() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change5_7)); }
	inline float get_change5_7() const { return ___change5_7; }
	inline float* get_address_of_change5_7() { return &___change5_7; }
	inline void set_change5_7(float value)
	{
		___change5_7 = value;
	}

	inline static int32_t get_offset_of_change6_8() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___change6_8)); }
	inline int32_t get_change6_8() const { return ___change6_8; }
	inline int32_t* get_address_of_change6_8() { return &___change6_8; }
	inline void set_change6_8(int32_t value)
	{
		___change6_8 = value;
	}

	inline static int32_t get_offset_of_mainscript_9() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___mainscript_9)); }
	inline Main_t2227614074 * get_mainscript_9() const { return ___mainscript_9; }
	inline Main_t2227614074 ** get_address_of_mainscript_9() { return &___mainscript_9; }
	inline void set_mainscript_9(Main_t2227614074 * value)
	{
		___mainscript_9 = value;
		Il2CppCodeGenWriteBarrier((&___mainscript_9), value);
	}

	inline static int32_t get_offset_of_betplacenames_10() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061, ___betplacenames_10)); }
	inline List_1_t3319525431 * get_betplacenames_10() const { return ___betplacenames_10; }
	inline List_1_t3319525431 ** get_address_of_betplacenames_10() { return &___betplacenames_10; }
	inline void set_betplacenames_10(List_1_t3319525431 * value)
	{
		___betplacenames_10 = value;
		Il2CppCodeGenWriteBarrier((&___betplacenames_10), value);
	}
};

struct GuiLabels_t593977061_StaticFields
{
public:
	// System.Int32 GuiLabels::GUIdepth
	int32_t ___GUIdepth_2;

public:
	inline static int32_t get_offset_of_GUIdepth_2() { return static_cast<int32_t>(offsetof(GuiLabels_t593977061_StaticFields, ___GUIdepth_2)); }
	inline int32_t get_GUIdepth_2() const { return ___GUIdepth_2; }
	inline int32_t* get_address_of_GUIdepth_2() { return &___GUIdepth_2; }
	inline void set_GUIdepth_2(int32_t value)
	{
		___GUIdepth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILABELS_T593977061_H
#ifndef MAIN_T2227614074_H
#define MAIN_T2227614074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2227614074  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Main::shade_alpha
	float ___shade_alpha_2;
	// System.Single Main::change1
	float ___change1_5;
	// System.Single Main::change2
	float ___change2_6;
	// System.Single Main::change3
	float ___change3_7;
	// System.Single Main::change4
	float ___change4_8;
	// System.Single Main::change5
	float ___change5_9;
	// System.Int32 Main::change6
	int32_t ___change6_10;
	// System.Boolean Main::click_once
	bool ___click_once_11;
	// System.Boolean Main::chip_notpressed
	bool ___chip_notpressed_12;
	// UnityEngine.Texture2D Main::Template
	Texture2D_t3840446185 * ___Template_13;
	// UnityEngine.Texture2D Main::shade
	Texture2D_t3840446185 * ___shade_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Main::sceneobjects
	List_1_t2585711361 * ___sceneobjects_15;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::horseracers_images
	List_1_t1017553631 * ___horseracers_images_16;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::bet_btns
	List_1_t1017553631 * ___bet_btns_17;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Main::chips_images
	List_1_t1017553631 * ___chips_images_18;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_win
	List_1_t2869341516 * ___horse_bets_win_19;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_place
	List_1_t2869341516 * ___horse_bets_place_20;
	// System.Collections.Generic.List`1<System.Single> Main::horse_bets_show
	List_1_t2869341516 * ___horse_bets_show_21;
	// System.Collections.Generic.List`1<System.Int32> Main::horses_finish
	List_1_t128053199 * ___horses_finish_22;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_win
	List_1_t2869341516 * ___current_bets_win_23;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_place
	List_1_t2869341516 * ___current_bets_place_24;
	// System.Collections.Generic.List`1<System.Single> Main::current_bets_show
	List_1_t2869341516 * ___current_bets_show_25;
	// System.Collections.Generic.List`1<System.String> Main::horseracers_names
	List_1_t3319525431 * ___horseracers_names_26;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>> Main::bet_odds_master
	List_1_t46448962 * ___bet_odds_master_27;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>> Main::current_bets_master
	List_1_t46448962 * ___current_bets_master_28;
	// Main/Selection Main::currentSel
	int32_t ___currentSel_29;
	// System.Single Main::stake
	float ___stake_30;
	// System.Single Main::visual_timer
	float ___visual_timer_31;
	// System.Int32 Main::current_bet_type
	int32_t ___current_bet_type_32;

public:
	inline static int32_t get_offset_of_shade_alpha_2() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___shade_alpha_2)); }
	inline float get_shade_alpha_2() const { return ___shade_alpha_2; }
	inline float* get_address_of_shade_alpha_2() { return &___shade_alpha_2; }
	inline void set_shade_alpha_2(float value)
	{
		___shade_alpha_2 = value;
	}

	inline static int32_t get_offset_of_change1_5() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change1_5)); }
	inline float get_change1_5() const { return ___change1_5; }
	inline float* get_address_of_change1_5() { return &___change1_5; }
	inline void set_change1_5(float value)
	{
		___change1_5 = value;
	}

	inline static int32_t get_offset_of_change2_6() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change2_6)); }
	inline float get_change2_6() const { return ___change2_6; }
	inline float* get_address_of_change2_6() { return &___change2_6; }
	inline void set_change2_6(float value)
	{
		___change2_6 = value;
	}

	inline static int32_t get_offset_of_change3_7() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change3_7)); }
	inline float get_change3_7() const { return ___change3_7; }
	inline float* get_address_of_change3_7() { return &___change3_7; }
	inline void set_change3_7(float value)
	{
		___change3_7 = value;
	}

	inline static int32_t get_offset_of_change4_8() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change4_8)); }
	inline float get_change4_8() const { return ___change4_8; }
	inline float* get_address_of_change4_8() { return &___change4_8; }
	inline void set_change4_8(float value)
	{
		___change4_8 = value;
	}

	inline static int32_t get_offset_of_change5_9() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change5_9)); }
	inline float get_change5_9() const { return ___change5_9; }
	inline float* get_address_of_change5_9() { return &___change5_9; }
	inline void set_change5_9(float value)
	{
		___change5_9 = value;
	}

	inline static int32_t get_offset_of_change6_10() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___change6_10)); }
	inline int32_t get_change6_10() const { return ___change6_10; }
	inline int32_t* get_address_of_change6_10() { return &___change6_10; }
	inline void set_change6_10(int32_t value)
	{
		___change6_10 = value;
	}

	inline static int32_t get_offset_of_click_once_11() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___click_once_11)); }
	inline bool get_click_once_11() const { return ___click_once_11; }
	inline bool* get_address_of_click_once_11() { return &___click_once_11; }
	inline void set_click_once_11(bool value)
	{
		___click_once_11 = value;
	}

	inline static int32_t get_offset_of_chip_notpressed_12() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___chip_notpressed_12)); }
	inline bool get_chip_notpressed_12() const { return ___chip_notpressed_12; }
	inline bool* get_address_of_chip_notpressed_12() { return &___chip_notpressed_12; }
	inline void set_chip_notpressed_12(bool value)
	{
		___chip_notpressed_12 = value;
	}

	inline static int32_t get_offset_of_Template_13() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___Template_13)); }
	inline Texture2D_t3840446185 * get_Template_13() const { return ___Template_13; }
	inline Texture2D_t3840446185 ** get_address_of_Template_13() { return &___Template_13; }
	inline void set_Template_13(Texture2D_t3840446185 * value)
	{
		___Template_13 = value;
		Il2CppCodeGenWriteBarrier((&___Template_13), value);
	}

	inline static int32_t get_offset_of_shade_14() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___shade_14)); }
	inline Texture2D_t3840446185 * get_shade_14() const { return ___shade_14; }
	inline Texture2D_t3840446185 ** get_address_of_shade_14() { return &___shade_14; }
	inline void set_shade_14(Texture2D_t3840446185 * value)
	{
		___shade_14 = value;
		Il2CppCodeGenWriteBarrier((&___shade_14), value);
	}

	inline static int32_t get_offset_of_sceneobjects_15() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___sceneobjects_15)); }
	inline List_1_t2585711361 * get_sceneobjects_15() const { return ___sceneobjects_15; }
	inline List_1_t2585711361 ** get_address_of_sceneobjects_15() { return &___sceneobjects_15; }
	inline void set_sceneobjects_15(List_1_t2585711361 * value)
	{
		___sceneobjects_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneobjects_15), value);
	}

	inline static int32_t get_offset_of_horseracers_images_16() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horseracers_images_16)); }
	inline List_1_t1017553631 * get_horseracers_images_16() const { return ___horseracers_images_16; }
	inline List_1_t1017553631 ** get_address_of_horseracers_images_16() { return &___horseracers_images_16; }
	inline void set_horseracers_images_16(List_1_t1017553631 * value)
	{
		___horseracers_images_16 = value;
		Il2CppCodeGenWriteBarrier((&___horseracers_images_16), value);
	}

	inline static int32_t get_offset_of_bet_btns_17() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___bet_btns_17)); }
	inline List_1_t1017553631 * get_bet_btns_17() const { return ___bet_btns_17; }
	inline List_1_t1017553631 ** get_address_of_bet_btns_17() { return &___bet_btns_17; }
	inline void set_bet_btns_17(List_1_t1017553631 * value)
	{
		___bet_btns_17 = value;
		Il2CppCodeGenWriteBarrier((&___bet_btns_17), value);
	}

	inline static int32_t get_offset_of_chips_images_18() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___chips_images_18)); }
	inline List_1_t1017553631 * get_chips_images_18() const { return ___chips_images_18; }
	inline List_1_t1017553631 ** get_address_of_chips_images_18() { return &___chips_images_18; }
	inline void set_chips_images_18(List_1_t1017553631 * value)
	{
		___chips_images_18 = value;
		Il2CppCodeGenWriteBarrier((&___chips_images_18), value);
	}

	inline static int32_t get_offset_of_horse_bets_win_19() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_win_19)); }
	inline List_1_t2869341516 * get_horse_bets_win_19() const { return ___horse_bets_win_19; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_win_19() { return &___horse_bets_win_19; }
	inline void set_horse_bets_win_19(List_1_t2869341516 * value)
	{
		___horse_bets_win_19 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_win_19), value);
	}

	inline static int32_t get_offset_of_horse_bets_place_20() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_place_20)); }
	inline List_1_t2869341516 * get_horse_bets_place_20() const { return ___horse_bets_place_20; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_place_20() { return &___horse_bets_place_20; }
	inline void set_horse_bets_place_20(List_1_t2869341516 * value)
	{
		___horse_bets_place_20 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_place_20), value);
	}

	inline static int32_t get_offset_of_horse_bets_show_21() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horse_bets_show_21)); }
	inline List_1_t2869341516 * get_horse_bets_show_21() const { return ___horse_bets_show_21; }
	inline List_1_t2869341516 ** get_address_of_horse_bets_show_21() { return &___horse_bets_show_21; }
	inline void set_horse_bets_show_21(List_1_t2869341516 * value)
	{
		___horse_bets_show_21 = value;
		Il2CppCodeGenWriteBarrier((&___horse_bets_show_21), value);
	}

	inline static int32_t get_offset_of_horses_finish_22() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horses_finish_22)); }
	inline List_1_t128053199 * get_horses_finish_22() const { return ___horses_finish_22; }
	inline List_1_t128053199 ** get_address_of_horses_finish_22() { return &___horses_finish_22; }
	inline void set_horses_finish_22(List_1_t128053199 * value)
	{
		___horses_finish_22 = value;
		Il2CppCodeGenWriteBarrier((&___horses_finish_22), value);
	}

	inline static int32_t get_offset_of_current_bets_win_23() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_win_23)); }
	inline List_1_t2869341516 * get_current_bets_win_23() const { return ___current_bets_win_23; }
	inline List_1_t2869341516 ** get_address_of_current_bets_win_23() { return &___current_bets_win_23; }
	inline void set_current_bets_win_23(List_1_t2869341516 * value)
	{
		___current_bets_win_23 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_win_23), value);
	}

	inline static int32_t get_offset_of_current_bets_place_24() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_place_24)); }
	inline List_1_t2869341516 * get_current_bets_place_24() const { return ___current_bets_place_24; }
	inline List_1_t2869341516 ** get_address_of_current_bets_place_24() { return &___current_bets_place_24; }
	inline void set_current_bets_place_24(List_1_t2869341516 * value)
	{
		___current_bets_place_24 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_place_24), value);
	}

	inline static int32_t get_offset_of_current_bets_show_25() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_show_25)); }
	inline List_1_t2869341516 * get_current_bets_show_25() const { return ___current_bets_show_25; }
	inline List_1_t2869341516 ** get_address_of_current_bets_show_25() { return &___current_bets_show_25; }
	inline void set_current_bets_show_25(List_1_t2869341516 * value)
	{
		___current_bets_show_25 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_show_25), value);
	}

	inline static int32_t get_offset_of_horseracers_names_26() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___horseracers_names_26)); }
	inline List_1_t3319525431 * get_horseracers_names_26() const { return ___horseracers_names_26; }
	inline List_1_t3319525431 ** get_address_of_horseracers_names_26() { return &___horseracers_names_26; }
	inline void set_horseracers_names_26(List_1_t3319525431 * value)
	{
		___horseracers_names_26 = value;
		Il2CppCodeGenWriteBarrier((&___horseracers_names_26), value);
	}

	inline static int32_t get_offset_of_bet_odds_master_27() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___bet_odds_master_27)); }
	inline List_1_t46448962 * get_bet_odds_master_27() const { return ___bet_odds_master_27; }
	inline List_1_t46448962 ** get_address_of_bet_odds_master_27() { return &___bet_odds_master_27; }
	inline void set_bet_odds_master_27(List_1_t46448962 * value)
	{
		___bet_odds_master_27 = value;
		Il2CppCodeGenWriteBarrier((&___bet_odds_master_27), value);
	}

	inline static int32_t get_offset_of_current_bets_master_28() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bets_master_28)); }
	inline List_1_t46448962 * get_current_bets_master_28() const { return ___current_bets_master_28; }
	inline List_1_t46448962 ** get_address_of_current_bets_master_28() { return &___current_bets_master_28; }
	inline void set_current_bets_master_28(List_1_t46448962 * value)
	{
		___current_bets_master_28 = value;
		Il2CppCodeGenWriteBarrier((&___current_bets_master_28), value);
	}

	inline static int32_t get_offset_of_currentSel_29() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___currentSel_29)); }
	inline int32_t get_currentSel_29() const { return ___currentSel_29; }
	inline int32_t* get_address_of_currentSel_29() { return &___currentSel_29; }
	inline void set_currentSel_29(int32_t value)
	{
		___currentSel_29 = value;
	}

	inline static int32_t get_offset_of_stake_30() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___stake_30)); }
	inline float get_stake_30() const { return ___stake_30; }
	inline float* get_address_of_stake_30() { return &___stake_30; }
	inline void set_stake_30(float value)
	{
		___stake_30 = value;
	}

	inline static int32_t get_offset_of_visual_timer_31() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___visual_timer_31)); }
	inline float get_visual_timer_31() const { return ___visual_timer_31; }
	inline float* get_address_of_visual_timer_31() { return &___visual_timer_31; }
	inline void set_visual_timer_31(float value)
	{
		___visual_timer_31 = value;
	}

	inline static int32_t get_offset_of_current_bet_type_32() { return static_cast<int32_t>(offsetof(Main_t2227614074, ___current_bet_type_32)); }
	inline int32_t get_current_bet_type_32() const { return ___current_bet_type_32; }
	inline int32_t* get_address_of_current_bet_type_32() { return &___current_bet_type_32; }
	inline void set_current_bet_type_32(int32_t value)
	{
		___current_bet_type_32 = value;
	}
};

struct Main_t2227614074_StaticFields
{
public:
	// System.Int32 Main::chip_gui_depth
	int32_t ___chip_gui_depth_3;
	// System.Int32 Main::gui_depth
	int32_t ___gui_depth_4;

public:
	inline static int32_t get_offset_of_chip_gui_depth_3() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___chip_gui_depth_3)); }
	inline int32_t get_chip_gui_depth_3() const { return ___chip_gui_depth_3; }
	inline int32_t* get_address_of_chip_gui_depth_3() { return &___chip_gui_depth_3; }
	inline void set_chip_gui_depth_3(int32_t value)
	{
		___chip_gui_depth_3 = value;
	}

	inline static int32_t get_offset_of_gui_depth_4() { return static_cast<int32_t>(offsetof(Main_t2227614074_StaticFields, ___gui_depth_4)); }
	inline int32_t get_gui_depth_4() const { return ___gui_depth_4; }
	inline int32_t* get_address_of_gui_depth_4() { return &___gui_depth_4; }
	inline void set_gui_depth_4(int32_t value)
	{
		___gui_depth_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T2227614074_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
extern "C"  float List_1_get_Item_m718437397_gshared (List_1_t2869341516 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
extern "C"  void List_1__ctor_m3152364487_gshared (List_1_t2869341516 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1628857705_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m2269680114_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m697420525_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
extern "C"  int32_t List_1_get_Count_m4125879936_gshared (List_1_t2869341516 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m186164705_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m888956288_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2770200702_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
extern "C"  void List_1_Clear_m3978655417_gshared (List_1_t2869341516 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
extern "C"  void List_1_Add_m2024781851_gshared (List_1_t2869341516 * __this, float p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m722690934_gshared (List_1_t2869341516 * __this, int32_t p0, float p1, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m706204246(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>::get_Item(System.Int32)
#define List_1_get_Item_m2670531749(__this, p0, method) ((  List_1_t2869341516 * (*) (List_1_t46448962 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
#define List_1_get_Item_m718437397(__this, p0, method) ((  float (*) (List_1_t2869341516 *, int32_t, const RuntimeMethod*))List_1_get_Item_m718437397_gshared)(__this, p0, method)
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m3489843083 (float* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_depth(System.Int32)
extern "C"  void GUI_set_depth_m2938673509 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_contentColor(UnityEngine.Color)
extern "C"  void GUI_set_contentColor_m1403854338 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C"  GUISkin_t1244372282 * GUI_get_skin_m1874615010 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C"  GUIStyle_t3956901511 * GUISkin_get_label_m1693050720 (GUISkin_t1244372282 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C"  void GUIStyle_set_fontSize_m1566850023 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String GuiLabels::bet_Value_topdisplay(System.Int32,System.Int32)
extern "C"  String_t* GuiLabels_bet_Value_topdisplay_m2233024845 (GuiLabels_t593977061 * __this, int32_t ___betype0, int32_t ___betnum1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern "C"  void GUI_Label_m2454565404 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture2D>::get_Count()
#define List_1_get_Count_m1676150876(__this, method) ((  int32_t (*) (List_1_t1017553631 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m2840606808(__this, p0, method) ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Texture2D>::get_Item(System.Int32)
#define List_1_get_Item_m3379783094(__this, p0, method) ((  Texture2D_t3840446185 * (*) (List_1_t1017553631 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void GUI_DrawTexture_m3124770796 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m3526297712(__this, method) ((  void (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture2D>::.ctor()
#define List_1__ctor_m90174447(__this, method) ((  void (*) (List_1_t1017553631 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
#define List_1__ctor_m3152364487(__this, method) ((  void (*) (List_1_t2869341516 *, const RuntimeMethod*))List_1__ctor_m3152364487_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1628857705(__this, method) ((  void (*) (List_1_t128053199 *, const RuntimeMethod*))List_1__ctor_m1628857705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>::.ctor()
#define List_1__ctor_m1412855716(__this, method) ((  void (*) (List_1_t46448962 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>::Add(!0)
#define List_1_Add_m3328803046(__this, p0, method) ((  void (*) (List_1_t46448962 *, List_1_t2869341516 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void Main::reset_bets()
extern "C"  void Main_reset_bets_m1112432460 (Main_t2227614074 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Main::randomwinners()
extern "C"  void Main_randomwinners_m793992468 (Main_t2227614074 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
#define List_1_Clear_m2269680114(__this, method) ((  void (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_Clear_m2269680114_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m697420525(__this, p0, method) ((  void (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_Add_m697420525_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
#define List_1_get_Count_m4125879936(__this, method) ((  int32_t (*) (List_1_t2869341516 *, const RuntimeMethod*))List_1_get_Count_m4125879936_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m186164705(__this, method) ((  int32_t (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_get_Count_m186164705_gshared)(__this, method)
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m888956288(__this, p0, method) ((  int32_t (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_get_Item_m888956288_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2770200702(__this, p0, method) ((  void (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m2770200702_gshared)(__this, p0, method)
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Single>>::get_Count()
#define List_1_get_Count_m774866856(__this, method) ((  int32_t (*) (List_1_t46448962 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void Main/<Timer>c__Iterator0::.ctor()
extern "C"  void U3CTimerU3Ec__Iterator0__ctor_m1791513085 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
#define List_1_Clear_m3978655417(__this, method) ((  void (*) (List_1_t2869341516 *, const RuntimeMethod*))List_1_Clear_m3978655417_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
#define List_1_Add_m2024781851(__this, p0, method) ((  void (*) (List_1_t2869341516 *, float, const RuntimeMethod*))List_1_Add_m2024781851_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,!0)
#define List_1_set_Item_m722690934(__this, p0, p1, method) ((  void (*) (List_1_t2869341516 *, int32_t, float, const RuntimeMethod*))List_1_set_Item_m722690934_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1518979886 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Calculate_wins()
extern "C"  void Main_Calculate_wins_m1407021724 (Main_t2227614074 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C"  Event_t2956885303 * Event_get_current_m2393892120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C"  Vector2_t2156229523  Event_get_mousePosition_m733809635 (Event_t2956885303 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3188543637 (Rect_t2360479859 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m2924350851 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Main::updatebet(System.Int32,System.Int32)
extern "C"  void Main_updatebet_m529302127 (Main_t2227614074 * __this, int32_t ___bet_type0, int32_t ___bet_ID1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Main::typeofbutton(System.Int32,System.Int32)
extern "C"  Texture2D_t3840446185 * Main_typeofbutton_m2032441888 (Main_t2227614074 * __this, int32_t ___betype0, int32_t ___betnum1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C"  void GUI_set_color_m1028198571 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern "C"  GUIStyle_t3956901511 * GUIStyle_get_none_m1545577352 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  bool GUI_Button_m2223708732 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, GUIStyle_t3956901511 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GuiLabels::.ctor()
extern "C"  void GuiLabels__ctor_m3778019952 (GuiLabels_t593977061 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GuiLabels__ctor_m3778019952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_betplacenames_10(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GuiLabels::Start()
extern "C"  void GuiLabels_Start_m3203687160 (GuiLabels_t593977061 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GuiLabels_Start_m3203687160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2227614074_il2cpp_TypeInfo_var);
		((Main_t2227614074_StaticFields*)il2cpp_codegen_static_fields_for(Main_t2227614074_il2cpp_TypeInfo_var))->set_gui_depth_4(1);
		return;
	}
}
// System.Void GuiLabels::Gui_Change()
extern "C"  void GuiLabels_Gui_Change_m3083760333 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GuiLabels_Gui_Change_m3083760333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t2227614074_il2cpp_TypeInfo_var);
		((Main_t2227614074_StaticFields*)il2cpp_codegen_static_fields_for(Main_t2227614074_il2cpp_TypeInfo_var))->set_chip_gui_depth_3((-1));
		((Main_t2227614074_StaticFields*)il2cpp_codegen_static_fields_for(Main_t2227614074_il2cpp_TypeInfo_var))->set_gui_depth_4((-1));
		return;
	}
}
// System.String GuiLabels::bet_Value_topdisplay(System.Int32,System.Int32)
extern "C"  String_t* GuiLabels_bet_Value_topdisplay_m2233024845 (GuiLabels_t593977061 * __this, int32_t ___betype0, int32_t ___betnum1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GuiLabels_bet_Value_topdisplay_m2233024845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Main_t2227614074 * L_0 = __this->get_mainscript_9();
		NullCheck(L_0);
		List_1_t46448962 * L_1 = L_0->get_current_bets_master_28();
		int32_t L_2 = ___betype0;
		NullCheck(L_1);
		List_1_t2869341516 * L_3 = List_1_get_Item_m2670531749(L_1, L_2, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_4 = ___betnum1;
		NullCheck(L_3);
		float L_5 = List_1_get_Item_m718437397(L_3, L_4, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		if ((!(((float)L_5) < ((float)(1.0f)))))
		{
			goto IL_0046;
		}
	}
	{
		Main_t2227614074 * L_6 = __this->get_mainscript_9();
		NullCheck(L_6);
		List_1_t46448962 * L_7 = L_6->get_bet_odds_master_27();
		int32_t L_8 = ___betype0;
		NullCheck(L_7);
		List_1_t2869341516 * L_9 = List_1_get_Item_m2670531749(L_7, L_8, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_10 = ___betnum1;
		NullCheck(L_9);
		float L_11 = List_1_get_Item_m718437397(L_9, L_10, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		V_0 = L_11;
		String_t* L_12 = Single_ToString_m3489843083((&V_0), _stringLiteral3452614621, /*hidden argument*/NULL);
		return L_12;
	}

IL_0046:
	{
		Main_t2227614074 * L_13 = __this->get_mainscript_9();
		NullCheck(L_13);
		List_1_t46448962 * L_14 = L_13->get_current_bets_master_28();
		int32_t L_15 = ___betype0;
		NullCheck(L_14);
		List_1_t2869341516 * L_16 = List_1_get_Item_m2670531749(L_14, L_15, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_17 = ___betnum1;
		NullCheck(L_16);
		float L_18 = List_1_get_Item_m718437397(L_16, L_17, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		V_1 = L_18;
		String_t* L_19 = Single_ToString_m3489843083((&V_1), _stringLiteral3452614621, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void GuiLabels::OnGUI()
extern "C"  void GuiLabels_OnGUI_m1879780399 (GuiLabels_t593977061 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GuiLabels_OnGUI_m1879780399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GuiLabels_t593977061_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GuiLabels_t593977061_StaticFields*)il2cpp_codegen_static_fields_for(GuiLabels_t593977061_il2cpp_TypeInfo_var))->get_GUIdepth_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_depth_m2938673509(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0157;
	}

IL_0011:
	{
		Color_t2555686324  L_1 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUISkin_t1244372282 * L_2 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t3956901511 * L_3 = GUISkin_get_label_m1693050720(L_2, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_set_fontSize_m1566850023(L_3, ((int32_t)((int32_t)L_4/(int32_t)((int32_t)30))), /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)6)))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_6 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		int32_t L_11 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_12 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_14 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m2614021312((&L_15), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_6)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_7)))/(float)(34.31f))))), (float)((float)((float)(((float)((float)L_8)))/(float)(34.67f))))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_9)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_10))), (float)((float)((float)(((float)((float)L_11)))/(float)(16.19f))))))), (float)((float)((float)(((float)((float)L_12)))/(float)(123.6f))))), ((float)((float)(((float)((float)L_13)))/(float)(6.32f))), ((float)((float)(((float)((float)L_14)))/(float)(1.89f))), /*hidden argument*/NULL);
		Main_t2227614074 * L_16 = __this->get_mainscript_9();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_current_bet_type_32();
		int32_t L_18 = V_0;
		String_t* L_19 = GuiLabels_bet_Value_topdisplay_m2233024845(__this, L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		goto IL_0153;
	}

IL_00c1:
	{
		int32_t L_20 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_21 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_23 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_24 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		int32_t L_26 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_27 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_28 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_29 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m2614021312((&L_30), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_20)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_21)))/(float)(34.31f))))), (float)((float)((float)(((float)((float)L_22)))/(float)(34.67f))))), (float)((float)((float)(((float)((float)L_23)))/(float)(2.06f))))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_24)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)6))))), (float)((float)((float)(((float)((float)L_26)))/(float)(16.19f))))))), (float)((float)((float)(((float)((float)L_27)))/(float)(123.6f))))), ((float)((float)(((float)((float)L_28)))/(float)(6.32f))), ((float)((float)(((float)((float)L_29)))/(float)(1.89f))), /*hidden argument*/NULL);
		Main_t2227614074 * L_31 = __this->get_mainscript_9();
		NullCheck(L_31);
		int32_t L_32 = L_31->get_current_bet_type_32();
		int32_t L_33 = V_0;
		String_t* L_34 = GuiLabels_bet_Value_topdisplay_m2233024845(__this, L_32, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_30, L_34, /*hidden argument*/NULL);
	}

IL_0153:
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_0157:
	{
		int32_t L_36 = V_0;
		Main_t2227614074 * L_37 = __this->get_mainscript_9();
		NullCheck(L_37);
		List_1_t1017553631 * L_38 = L_37->get_horseracers_images_16();
		NullCheck(L_38);
		int32_t L_39 = List_1_get_Count_m1676150876(L_38, /*hidden argument*/List_1_get_Count_m1676150876_RuntimeMethod_var);
		if ((((int32_t)L_36) < ((int32_t)L_39)))
		{
			goto IL_0011;
		}
	}
	{
		V_1 = 0;
		goto IL_01e8;
	}

IL_0174:
	{
		int32_t L_40 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_41 = V_1;
		int32_t L_42 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_43 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_44 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_45 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_46 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_47 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Rect__ctor_m2614021312((&L_48), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_40)))/(float)(6.0f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_41))), (float)((float)((float)(((float)((float)L_42)))/(float)(4.52f))))))), (float)((float)((float)(((float)((float)L_43)))/(float)(23.0f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_44)))/(float)(1.096f))), (float)((float)((float)(((float)((float)L_45)))/(float)(35.43f))))), ((float)((float)(((float)((float)L_46)))/(float)(5.0f))), ((float)((float)(((float)((float)L_47)))/(float)(11.59f))), /*hidden argument*/NULL);
		List_1_t3319525431 * L_49 = __this->get_betplacenames_10();
		int32_t L_50 = V_1;
		NullCheck(L_49);
		String_t* L_51 = List_1_get_Item_m2840606808(L_49, L_50, /*hidden argument*/List_1_get_Item_m2840606808_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		int32_t L_52 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_01e8:
	{
		int32_t L_53 = V_1;
		if ((((int32_t)L_53) < ((int32_t)3)))
		{
			goto IL_0174;
		}
	}
	{
		Main_t2227614074 * L_54 = __this->get_mainscript_9();
		NullCheck(L_54);
		int32_t L_55 = L_54->get_currentSel_29();
		if ((!(((uint32_t)L_55) == ((uint32_t)1))))
		{
			goto IL_027c;
		}
	}
	{
		V_2 = 0;
		goto IL_0266;
	}

IL_0207:
	{
		int32_t L_56 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_57 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_58 = V_2;
		int32_t L_59 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_61 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Rect__ctor_m2614021312((&L_62), ((float)((float)(((float)((float)L_56)))/(float)(1.25f))), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_57)))/(float)(1.107f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_58))), (float)((float)((float)(((float)((float)L_59)))/(float)(11.29f))))))), ((float)((float)(((float)((float)L_60)))/(float)(4.8f))), ((float)((float)(((float)((float)L_61)))/(float)(9.36f))), /*hidden argument*/NULL);
		Main_t2227614074 * L_63 = __this->get_mainscript_9();
		NullCheck(L_63);
		List_1_t1017553631 * L_64 = L_63->get_chips_images_18();
		int32_t L_65 = V_2;
		NullCheck(L_64);
		Texture2D_t3840446185 * L_66 = List_1_get_Item_m3379783094(L_64, L_65, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_62, L_66, /*hidden argument*/NULL);
		int32_t L_67 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
	}

IL_0266:
	{
		int32_t L_68 = V_2;
		Main_t2227614074 * L_69 = __this->get_mainscript_9();
		NullCheck(L_69);
		List_1_t1017553631 * L_70 = L_69->get_chips_images_18();
		NullCheck(L_70);
		int32_t L_71 = List_1_get_Count_m1676150876(L_70, /*hidden argument*/List_1_get_Count_m1676150876_RuntimeMethod_var);
		if ((((int32_t)L_68) < ((int32_t)L_71)))
		{
			goto IL_0207;
		}
	}

IL_027c:
	{
		return;
	}
}
// System.Void GuiLabels::.cctor()
extern "C"  void GuiLabels__cctor_m1284705889 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Main::.ctor()
extern "C"  void Main__ctor_m136889595 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main__ctor_m136889595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2585711361 * L_0 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m3526297712(L_0, /*hidden argument*/List_1__ctor_m3526297712_RuntimeMethod_var);
		__this->set_sceneobjects_15(L_0);
		List_1_t1017553631 * L_1 = (List_1_t1017553631 *)il2cpp_codegen_object_new(List_1_t1017553631_il2cpp_TypeInfo_var);
		List_1__ctor_m90174447(L_1, /*hidden argument*/List_1__ctor_m90174447_RuntimeMethod_var);
		__this->set_horseracers_images_16(L_1);
		List_1_t1017553631 * L_2 = (List_1_t1017553631 *)il2cpp_codegen_object_new(List_1_t1017553631_il2cpp_TypeInfo_var);
		List_1__ctor_m90174447(L_2, /*hidden argument*/List_1__ctor_m90174447_RuntimeMethod_var);
		__this->set_bet_btns_17(L_2);
		List_1_t1017553631 * L_3 = (List_1_t1017553631 *)il2cpp_codegen_object_new(List_1_t1017553631_il2cpp_TypeInfo_var);
		List_1__ctor_m90174447(L_3, /*hidden argument*/List_1__ctor_m90174447_RuntimeMethod_var);
		__this->set_chips_images_18(L_3);
		List_1_t2869341516 * L_4 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_4, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_horse_bets_win_19(L_4);
		List_1_t2869341516 * L_5 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_5, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_horse_bets_place_20(L_5);
		List_1_t2869341516 * L_6 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_6, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_horse_bets_show_21(L_6);
		List_1_t128053199 * L_7 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_7, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		__this->set_horses_finish_22(L_7);
		List_1_t2869341516 * L_8 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_8, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_current_bets_win_23(L_8);
		List_1_t2869341516 * L_9 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_9, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_current_bets_place_24(L_9);
		List_1_t2869341516 * L_10 = (List_1_t2869341516 *)il2cpp_codegen_object_new(List_1_t2869341516_il2cpp_TypeInfo_var);
		List_1__ctor_m3152364487(L_10, /*hidden argument*/List_1__ctor_m3152364487_RuntimeMethod_var);
		__this->set_current_bets_show_25(L_10);
		List_1_t3319525431 * L_11 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_11, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_horseracers_names_26(L_11);
		List_1_t46448962 * L_12 = (List_1_t46448962 *)il2cpp_codegen_object_new(List_1_t46448962_il2cpp_TypeInfo_var);
		List_1__ctor_m1412855716(L_12, /*hidden argument*/List_1__ctor_m1412855716_RuntimeMethod_var);
		__this->set_bet_odds_master_27(L_12);
		List_1_t46448962 * L_13 = (List_1_t46448962 *)il2cpp_codegen_object_new(List_1_t46448962_il2cpp_TypeInfo_var);
		List_1__ctor_m1412855716(L_13, /*hidden argument*/List_1__ctor_m1412855716_RuntimeMethod_var);
		__this->set_current_bets_master_28(L_13);
		__this->set_stake_30((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::Start()
extern "C"  void Main_Start_m1779287939 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Start_m1779287939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_currentSel_29(0);
		List_1_t46448962 * L_0 = __this->get_bet_odds_master_27();
		List_1_t2869341516 * L_1 = __this->get_horse_bets_win_19();
		NullCheck(L_0);
		List_1_Add_m3328803046(L_0, L_1, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		List_1_t46448962 * L_2 = __this->get_bet_odds_master_27();
		List_1_t2869341516 * L_3 = __this->get_horse_bets_place_20();
		NullCheck(L_2);
		List_1_Add_m3328803046(L_2, L_3, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		List_1_t46448962 * L_4 = __this->get_bet_odds_master_27();
		List_1_t2869341516 * L_5 = __this->get_horse_bets_show_21();
		NullCheck(L_4);
		List_1_Add_m3328803046(L_4, L_5, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		List_1_t46448962 * L_6 = __this->get_current_bets_master_28();
		List_1_t2869341516 * L_7 = __this->get_current_bets_win_23();
		NullCheck(L_6);
		List_1_Add_m3328803046(L_6, L_7, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		List_1_t46448962 * L_8 = __this->get_current_bets_master_28();
		List_1_t2869341516 * L_9 = __this->get_current_bets_place_24();
		NullCheck(L_8);
		List_1_Add_m3328803046(L_8, L_9, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		List_1_t46448962 * L_10 = __this->get_current_bets_master_28();
		List_1_t2869341516 * L_11 = __this->get_current_bets_show_25();
		NullCheck(L_10);
		List_1_Add_m3328803046(L_10, L_11, /*hidden argument*/List_1_Add_m3328803046_RuntimeMethod_var);
		Main_reset_bets_m1112432460(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GuiLabels_t593977061_il2cpp_TypeInfo_var);
		((GuiLabels_t593977061_StaticFields*)il2cpp_codegen_static_fields_for(GuiLabels_t593977061_il2cpp_TypeInfo_var))->set_GUIdepth_2(0);
		Main_randomwinners_m793992468(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main::Update()
extern "C"  void Main_Update_m160769253 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Main::randomwinners()
extern "C"  void Main_randomwinners_m793992468 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_randomwinners_m793992468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t128053199 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		List_1_t128053199 * L_0 = __this->get_horses_finish_22();
		NullCheck(L_0);
		List_1_Clear_m2269680114(L_0, /*hidden argument*/List_1_Clear_m2269680114_RuntimeMethod_var);
		List_1_t128053199 * L_1 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1628857705(L_1, /*hidden argument*/List_1__ctor_m1628857705_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0023;
	}

IL_0018:
	{
		List_1_t128053199 * L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		List_1_Add_m697420525(L_2, L_3, /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		List_1_t2869341516 * L_6 = __this->get_horse_bets_win_19();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m4125879936(L_6, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0018;
		}
	}
	{
		V_2 = 0;
		goto IL_006a;
	}

IL_003b:
	{
		List_1_t128053199 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m186164705(L_8, /*hidden argument*/List_1_get_Count_m186164705_RuntimeMethod_var);
		V_3 = L_9;
		int32_t L_10 = V_3;
		int32_t L_11 = Random_Range_m4054026115(NULL /*static, unused*/, 0, L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		List_1_t128053199 * L_12 = __this->get_horses_finish_22();
		List_1_t128053199 * L_13 = V_0;
		int32_t L_14 = V_4;
		NullCheck(L_13);
		int32_t L_15 = List_1_get_Item_m888956288(L_13, L_14, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		NullCheck(L_12);
		List_1_Add_m697420525(L_12, L_15, /*hidden argument*/List_1_Add_m697420525_RuntimeMethod_var);
		List_1_t128053199 * L_16 = V_0;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		List_1_RemoveAt_m2770200702(L_16, L_17, /*hidden argument*/List_1_RemoveAt_m2770200702_RuntimeMethod_var);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_006a:
	{
		int32_t L_19 = V_2;
		List_1_t2869341516 * L_20 = __this->get_horse_bets_win_19();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m4125879936(L_20, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_003b;
		}
	}
	{
		return;
	}
}
// System.Void Main::Calculate_wins()
extern "C"  void Main_Calculate_wins_m1407021724 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Calculate_wins_m1407021724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		List_1_t128053199 * L_0 = __this->get_horses_finish_22();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Item_m888956288(L_0, 0, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		V_0 = L_1;
		List_1_t128053199 * L_2 = __this->get_horses_finish_22();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Item_m888956288(L_2, 1, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		V_1 = L_3;
		List_1_t128053199 * L_4 = __this->get_horses_finish_22();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Item_m888956288(L_4, 2, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		V_2 = L_5;
		V_3 = (0.0f);
		V_4 = 0;
		goto IL_02ae;
	}

IL_0035:
	{
		V_5 = 0;
		goto IL_0296;
	}

IL_003d:
	{
		int32_t L_6 = V_4;
		if (L_6)
		{
			goto IL_00fa;
		}
	}
	{
		List_1_t46448962 * L_7 = __this->get_current_bets_master_28();
		int32_t L_8 = V_4;
		NullCheck(L_7);
		List_1_t2869341516 * L_9 = List_1_get_Item_m2670531749(L_7, L_8, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		float L_11 = List_1_get_Item_m718437397(L_9, L_10, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		if ((!(((float)L_11) > ((float)(0.0f)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_12 = V_5;
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_00fa;
		}
	}
	{
		float L_14 = V_3;
		List_1_t46448962 * L_15 = __this->get_current_bets_master_28();
		int32_t L_16 = V_4;
		NullCheck(L_15);
		List_1_t2869341516 * L_17 = List_1_get_Item_m2670531749(L_15, L_16, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_18 = V_0;
		NullCheck(L_17);
		float L_19 = List_1_get_Item_m718437397(L_17, L_18, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		List_1_t46448962 * L_20 = __this->get_bet_odds_master_27();
		int32_t L_21 = V_4;
		NullCheck(L_20);
		List_1_t2869341516 * L_22 = List_1_get_Item_m2670531749(L_20, L_21, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_23 = V_0;
		NullCheck(L_22);
		float L_24 = List_1_get_Item_m718437397(L_22, L_23, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		V_3 = ((float)il2cpp_codegen_add((float)L_14, (float)((float)il2cpp_codegen_multiply((float)L_19, (float)L_24))));
		ObjectU5BU5D_t2843939325* L_25 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral984239891);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral984239891);
		ObjectU5BU5D_t2843939325* L_26 = L_25;
		float L_27 = V_3;
		float L_28 = L_27;
		RuntimeObject * L_29 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_29);
		ObjectU5BU5D_t2843939325* L_30 = L_26;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral1111741867);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1111741867);
		ObjectU5BU5D_t2843939325* L_31 = L_30;
		List_1_t46448962 * L_32 = __this->get_bet_odds_master_27();
		int32_t L_33 = V_4;
		NullCheck(L_32);
		List_1_t2869341516 * L_34 = List_1_get_Item_m2670531749(L_32, L_33, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_35 = V_0;
		NullCheck(L_34);
		float L_36 = List_1_get_Item_m718437397(L_34, L_35, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_37 = L_36;
		RuntimeObject * L_38 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_38);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_38);
		ObjectU5BU5D_t2843939325* L_39 = L_31;
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, _stringLiteral1863184053);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1863184053);
		ObjectU5BU5D_t2843939325* L_40 = L_39;
		List_1_t46448962 * L_41 = __this->get_current_bets_master_28();
		int32_t L_42 = V_4;
		NullCheck(L_41);
		List_1_t2869341516 * L_43 = List_1_get_Item_m2670531749(L_41, L_42, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_44 = V_0;
		NullCheck(L_43);
		float L_45 = List_1_get_Item_m718437397(L_43, L_44, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_46 = L_45;
		RuntimeObject * L_47 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_47);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2971454694(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
	}

IL_00fa:
	{
		int32_t L_49 = V_4;
		if ((!(((uint32_t)L_49) == ((uint32_t)1))))
		{
			goto IL_0290;
		}
	}
	{
		List_1_t46448962 * L_50 = __this->get_current_bets_master_28();
		int32_t L_51 = V_4;
		NullCheck(L_50);
		List_1_t2869341516 * L_52 = List_1_get_Item_m2670531749(L_50, L_51, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_53 = V_0;
		NullCheck(L_52);
		float L_54 = List_1_get_Item_m718437397(L_52, L_53, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		if ((!(((float)L_54) > ((float)(0.0f)))))
		{
			goto IL_01c9;
		}
	}
	{
		int32_t L_55 = V_5;
		int32_t L_56 = V_0;
		if ((!(((uint32_t)L_55) == ((uint32_t)L_56))))
		{
			goto IL_01c9;
		}
	}
	{
		float L_57 = V_3;
		List_1_t46448962 * L_58 = __this->get_current_bets_master_28();
		int32_t L_59 = V_4;
		NullCheck(L_58);
		List_1_t2869341516 * L_60 = List_1_get_Item_m2670531749(L_58, L_59, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_61 = V_0;
		NullCheck(L_60);
		float L_62 = List_1_get_Item_m718437397(L_60, L_61, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		List_1_t46448962 * L_63 = __this->get_bet_odds_master_27();
		int32_t L_64 = V_4;
		NullCheck(L_63);
		List_1_t2869341516 * L_65 = List_1_get_Item_m2670531749(L_63, L_64, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_66 = V_0;
		NullCheck(L_65);
		float L_67 = List_1_get_Item_m718437397(L_65, L_66, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		V_3 = ((float)il2cpp_codegen_add((float)L_57, (float)((float)il2cpp_codegen_multiply((float)L_62, (float)L_67))));
		ObjectU5BU5D_t2843939325* L_68 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_68);
		ArrayElementTypeCheck (L_68, _stringLiteral984239891);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral984239891);
		ObjectU5BU5D_t2843939325* L_69 = L_68;
		float L_70 = V_3;
		float L_71 = L_70;
		RuntimeObject * L_72 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_72);
		ObjectU5BU5D_t2843939325* L_73 = L_69;
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, _stringLiteral1111741867);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1111741867);
		ObjectU5BU5D_t2843939325* L_74 = L_73;
		List_1_t46448962 * L_75 = __this->get_bet_odds_master_27();
		int32_t L_76 = V_4;
		NullCheck(L_75);
		List_1_t2869341516 * L_77 = List_1_get_Item_m2670531749(L_75, L_76, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_78 = V_0;
		NullCheck(L_77);
		float L_79 = List_1_get_Item_m718437397(L_77, L_78, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_80 = L_79;
		RuntimeObject * L_81 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_81);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_81);
		ObjectU5BU5D_t2843939325* L_82 = L_74;
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, _stringLiteral1863184053);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1863184053);
		ObjectU5BU5D_t2843939325* L_83 = L_82;
		List_1_t46448962 * L_84 = __this->get_current_bets_master_28();
		int32_t L_85 = V_4;
		NullCheck(L_84);
		List_1_t2869341516 * L_86 = List_1_get_Item_m2670531749(L_84, L_85, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_87 = V_0;
		NullCheck(L_86);
		float L_88 = List_1_get_Item_m718437397(L_86, L_87, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_89 = L_88;
		RuntimeObject * L_90 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_89);
		NullCheck(L_83);
		ArrayElementTypeCheck (L_83, L_90);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_90);
		ObjectU5BU5D_t2843939325* L_91 = L_83;
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, _stringLiteral2028776514);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2028776514);
		ObjectU5BU5D_t2843939325* L_92 = L_91;
		int32_t L_93 = V_0;
		int32_t L_94 = L_93;
		RuntimeObject * L_95 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_94);
		NullCheck(L_92);
		ArrayElementTypeCheck (L_92, L_95);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_95);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = String_Concat_m2971454694(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
	}

IL_01c9:
	{
		List_1_t46448962 * L_97 = __this->get_current_bets_master_28();
		int32_t L_98 = V_4;
		NullCheck(L_97);
		List_1_t2869341516 * L_99 = List_1_get_Item_m2670531749(L_97, L_98, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_100 = V_1;
		NullCheck(L_99);
		float L_101 = List_1_get_Item_m718437397(L_99, L_100, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		if ((!(((float)L_101) > ((float)(0.0f)))))
		{
			goto IL_0290;
		}
	}
	{
		int32_t L_102 = V_5;
		int32_t L_103 = V_1;
		if ((!(((uint32_t)L_102) == ((uint32_t)L_103))))
		{
			goto IL_0290;
		}
	}
	{
		float L_104 = V_3;
		List_1_t46448962 * L_105 = __this->get_current_bets_master_28();
		int32_t L_106 = V_4;
		NullCheck(L_105);
		List_1_t2869341516 * L_107 = List_1_get_Item_m2670531749(L_105, L_106, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_108 = V_1;
		NullCheck(L_107);
		float L_109 = List_1_get_Item_m718437397(L_107, L_108, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		List_1_t46448962 * L_110 = __this->get_bet_odds_master_27();
		int32_t L_111 = V_4;
		NullCheck(L_110);
		List_1_t2869341516 * L_112 = List_1_get_Item_m2670531749(L_110, L_111, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_113 = V_1;
		NullCheck(L_112);
		float L_114 = List_1_get_Item_m718437397(L_112, L_113, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		V_3 = ((float)il2cpp_codegen_add((float)L_104, (float)((float)il2cpp_codegen_multiply((float)L_109, (float)L_114))));
		ObjectU5BU5D_t2843939325* L_115 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_115);
		ArrayElementTypeCheck (L_115, _stringLiteral984239891);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral984239891);
		ObjectU5BU5D_t2843939325* L_116 = L_115;
		float L_117 = V_3;
		float L_118 = L_117;
		RuntimeObject * L_119 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_118);
		NullCheck(L_116);
		ArrayElementTypeCheck (L_116, L_119);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_119);
		ObjectU5BU5D_t2843939325* L_120 = L_116;
		NullCheck(L_120);
		ArrayElementTypeCheck (L_120, _stringLiteral1111741867);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1111741867);
		ObjectU5BU5D_t2843939325* L_121 = L_120;
		List_1_t46448962 * L_122 = __this->get_bet_odds_master_27();
		int32_t L_123 = V_4;
		NullCheck(L_122);
		List_1_t2869341516 * L_124 = List_1_get_Item_m2670531749(L_122, L_123, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_125 = V_1;
		NullCheck(L_124);
		float L_126 = List_1_get_Item_m718437397(L_124, L_125, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_127 = L_126;
		RuntimeObject * L_128 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_127);
		NullCheck(L_121);
		ArrayElementTypeCheck (L_121, L_128);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_128);
		ObjectU5BU5D_t2843939325* L_129 = L_121;
		NullCheck(L_129);
		ArrayElementTypeCheck (L_129, _stringLiteral1863184053);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1863184053);
		ObjectU5BU5D_t2843939325* L_130 = L_129;
		List_1_t46448962 * L_131 = __this->get_current_bets_master_28();
		int32_t L_132 = V_4;
		NullCheck(L_131);
		List_1_t2869341516 * L_133 = List_1_get_Item_m2670531749(L_131, L_132, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_134 = V_1;
		NullCheck(L_133);
		float L_135 = List_1_get_Item_m718437397(L_133, L_134, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_136 = L_135;
		RuntimeObject * L_137 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_136);
		NullCheck(L_130);
		ArrayElementTypeCheck (L_130, L_137);
		(L_130)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_137);
		ObjectU5BU5D_t2843939325* L_138 = L_130;
		NullCheck(L_138);
		ArrayElementTypeCheck (L_138, _stringLiteral2028776514);
		(L_138)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2028776514);
		ObjectU5BU5D_t2843939325* L_139 = L_138;
		int32_t L_140 = V_1;
		int32_t L_141 = L_140;
		RuntimeObject * L_142 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_141);
		NullCheck(L_139);
		ArrayElementTypeCheck (L_139, L_142);
		(L_139)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_142);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_143 = String_Concat_m2971454694(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
	}

IL_0290:
	{
		int32_t L_144 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_144, (int32_t)1));
	}

IL_0296:
	{
		int32_t L_145 = V_5;
		List_1_t2869341516 * L_146 = __this->get_current_bets_win_23();
		NullCheck(L_146);
		int32_t L_147 = List_1_get_Count_m4125879936(L_146, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		if ((((int32_t)L_145) < ((int32_t)L_147)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_148 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_148, (int32_t)1));
	}

IL_02ae:
	{
		int32_t L_149 = V_4;
		List_1_t46448962 * L_150 = __this->get_current_bets_master_28();
		NullCheck(L_150);
		int32_t L_151 = List_1_get_Count_m774866856(L_150, /*hidden argument*/List_1_get_Count_m774866856_RuntimeMethod_var);
		if ((((int32_t)L_149) < ((int32_t)L_151)))
		{
			goto IL_0035;
		}
	}
	{
		return;
	}
}
// System.Collections.IEnumerator Main::Timer(System.Single)
extern "C"  RuntimeObject* Main_Timer_m2167688892 (Main_t2227614074 * __this, float ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Timer_m2167688892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTimerU3Ec__Iterator0_t685532230 * V_0 = NULL;
	{
		U3CTimerU3Ec__Iterator0_t685532230 * L_0 = (U3CTimerU3Ec__Iterator0_t685532230 *)il2cpp_codegen_object_new(U3CTimerU3Ec__Iterator0_t685532230_il2cpp_TypeInfo_var);
		U3CTimerU3Ec__Iterator0__ctor_m1791513085(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerU3Ec__Iterator0_t685532230 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CTimerU3Ec__Iterator0_t685532230 * L_3 = V_0;
		return L_3;
	}
}
// System.Void Main::reset_bets()
extern "C"  void Main_reset_bets_m1112432460 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_reset_bets_m1112432460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_004e;
	}

IL_0007:
	{
		List_1_t46448962 * L_0 = __this->get_current_bets_master_28();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		List_1_t2869341516 * L_2 = List_1_get_Item_m2670531749(L_0, L_1, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		NullCheck(L_2);
		List_1_Clear_m3978655417(L_2, /*hidden argument*/List_1_Clear_m3978655417_RuntimeMethod_var);
		V_1 = 0;
		goto IL_0039;
	}

IL_001f:
	{
		List_1_t46448962 * L_3 = __this->get_current_bets_master_28();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		List_1_t2869341516 * L_5 = List_1_get_Item_m2670531749(L_3, L_4, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		NullCheck(L_5);
		List_1_Add_m2024781851(L_5, (0.0f), /*hidden argument*/List_1_Add_m2024781851_RuntimeMethod_var);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_1;
		List_1_t2869341516 * L_8 = __this->get_horse_bets_win_19();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m4125879936(L_8, /*hidden argument*/List_1_get_Count_m4125879936_RuntimeMethod_var);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_0;
		List_1_t46448962 * L_12 = __this->get_bet_odds_master_27();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m774866856(L_12, /*hidden argument*/List_1_get_Count_m774866856_RuntimeMethod_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Main::updatebet(System.Int32,System.Int32)
extern "C"  void Main_updatebet_m529302127 (Main_t2227614074 * __this, int32_t ___bet_type0, int32_t ___bet_ID1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_updatebet_m529302127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2869341516 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t46448962 * L_0 = __this->get_current_bets_master_28();
		int32_t L_1 = ___bet_type0;
		NullCheck(L_0);
		List_1_t2869341516 * L_2 = List_1_get_Item_m2670531749(L_0, L_1, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		List_1_t2869341516 * L_3 = L_2;
		V_0 = L_3;
		int32_t L_4 = ___bet_ID1;
		int32_t L_5 = L_4;
		V_1 = L_5;
		List_1_t2869341516 * L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		float L_8 = List_1_get_Item_m718437397(L_6, L_7, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		float L_9 = __this->get_stake_30();
		NullCheck(L_3);
		List_1_set_Item_m722690934(L_3, L_5, ((float)il2cpp_codegen_add((float)L_8, (float)L_9)), /*hidden argument*/List_1_set_Item_m722690934_RuntimeMethod_var);
		return;
	}
}
// UnityEngine.Texture2D Main::typeofbutton(System.Int32,System.Int32)
extern "C"  Texture2D_t3840446185 * Main_typeofbutton_m2032441888 (Main_t2227614074 * __this, int32_t ___betype0, int32_t ___betnum1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_typeofbutton_m2032441888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t46448962 * L_0 = __this->get_current_bets_master_28();
		int32_t L_1 = ___betype0;
		NullCheck(L_0);
		List_1_t2869341516 * L_2 = List_1_get_Item_m2670531749(L_0, L_1, /*hidden argument*/List_1_get_Item_m2670531749_RuntimeMethod_var);
		int32_t L_3 = ___betnum1;
		NullCheck(L_2);
		float L_4 = List_1_get_Item_m718437397(L_2, L_3, /*hidden argument*/List_1_get_Item_m718437397_RuntimeMethod_var);
		if ((!(((float)L_4) < ((float)(1.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1017553631 * L_5 = __this->get_bet_btns_17();
		NullCheck(L_5);
		Texture2D_t3840446185 * L_6 = List_1_get_Item_m3379783094(L_5, 0, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		return L_6;
	}

IL_0029:
	{
		List_1_t1017553631 * L_7 = __this->get_bet_btns_17();
		NullCheck(L_7);
		Texture2D_t3840446185 * L_8 = List_1_get_Item_m3379783094(L_7, 2, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		return L_8;
	}
}
// System.Void Main::OnGUI()
extern "C"  void Main_OnGUI_m1948052770 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_OnGUI_m1948052770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Rect_t2360479859  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t2360479859  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	Rect_t2360479859  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		int32_t L_0 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m2614021312((&L_2), (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_3 = __this->get_Template_13();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Rect_t2360479859  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m2614021312((&L_4), (0.0f), (0.0f), (100.0f), (100.0f), /*hidden argument*/NULL);
		bool L_5 = GUI_Button_m1518979886(NULL /*static, unused*/, L_4, _stringLiteral1130393540, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		Main_Calculate_wins_m1407021724(__this, /*hidden argument*/NULL);
	}

IL_0054:
	{
		V_0 = 0;
		goto IL_014d;
	}

IL_005b:
	{
		int32_t L_6 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		int32_t L_8 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m2614021312((&L_12), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_6)))/(float)(6.0f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_7))), (float)((float)((float)(((float)((float)L_8)))/(float)(4.52f))))))), ((float)((float)(((float)((float)L_9)))/(float)(1.096f))), ((float)((float)(((float)((float)L_10)))/(float)(5.0f))), ((float)((float)(((float)((float)L_11)))/(float)(11.59f))), /*hidden argument*/NULL);
		List_1_t1017553631 * L_13 = __this->get_bet_btns_17();
		NullCheck(L_13);
		Texture2D_t3840446185 * L_14 = List_1_get_Item_m3379783094(L_13, 1, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		int32_t L_15 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_1), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_15)))/(float)(6.0f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_16))), (float)((float)((float)(((float)((float)L_17)))/(float)(4.52f))))))), ((float)((float)(((float)((float)L_18)))/(float)(1.096f))), ((float)((float)(((float)((float)L_19)))/(float)(5.0f))), ((float)((float)(((float)((float)L_20)))/(float)(11.59f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_21 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0137;
		}
	}
	{
		int32_t L_22 = __this->get_currentSel_29();
		if ((((int32_t)L_22) == ((int32_t)1)))
		{
			goto IL_0137;
		}
	}
	{
		Event_t2956885303 * L_23 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector2_t2156229523  L_24 = Event_get_mousePosition_m733809635(L_23, /*hidden argument*/NULL);
		bool L_25 = Rect_Contains_m3188543637((&V_1), L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0137;
		}
	}
	{
		bool L_26 = __this->get_click_once_11();
		if (L_26)
		{
			goto IL_0137;
		}
	}
	{
		int32_t L_27 = V_0;
		__this->set_current_bet_type_32(L_27);
	}

IL_0137:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_28 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0149;
		}
	}
	{
		__this->set_click_once_11((bool)0);
	}

IL_0149:
	{
		int32_t L_29 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_014d:
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) < ((int32_t)3)))
		{
			goto IL_005b;
		}
	}
	{
		V_2 = 0;
		goto IL_05ab;
	}

IL_015b:
	{
		int32_t L_31 = V_2;
		if ((((int32_t)L_31) >= ((int32_t)6)))
		{
			goto IL_0383;
		}
	}
	{
		int32_t L_32 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_33 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_34 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_35 = V_2;
		int32_t L_36 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_37 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_38 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_3), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_32)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_33)))/(float)(34.31f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_34)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_35))), (float)((float)((float)(((float)((float)L_36)))/(float)(16.19f))))))), ((float)((float)(((float)((float)L_37)))/(float)(5.8f))), ((float)((float)(((float)((float)L_38)))/(float)(19.8f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_39 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0217;
		}
	}
	{
		int32_t L_40 = __this->get_currentSel_29();
		if ((((int32_t)L_40) == ((int32_t)1)))
		{
			goto IL_0217;
		}
	}
	{
		Event_t2956885303 * L_41 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector2_t2156229523  L_42 = Event_get_mousePosition_m733809635(L_41, /*hidden argument*/NULL);
		bool L_43 = Rect_Contains_m3188543637((&V_3), L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0217;
		}
	}
	{
		bool L_44 = __this->get_click_once_11();
		if (L_44)
		{
			goto IL_0217;
		}
	}
	{
		int32_t L_45 = V_2;
		int32_t L_46 = L_45;
		RuntimeObject * L_47 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1945711025, L_47, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_current_bet_type_32();
		int32_t L_50 = V_2;
		Main_updatebet_m529302127(__this, L_49, L_50, /*hidden argument*/NULL);
		__this->set_click_once_11((bool)1);
	}

IL_0217:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_51 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0229;
		}
	}
	{
		__this->set_click_once_11((bool)0);
	}

IL_0229:
	{
		Color_t2555686324  L_52 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		int32_t L_53 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_54 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_55 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_56 = V_2;
		int32_t L_57 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_58 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_59 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_60;
		memset(&L_60, 0, sizeof(L_60));
		Rect__ctor_m2614021312((&L_60), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_53)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_54)))/(float)(34.31f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_55)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_56))), (float)((float)((float)(((float)((float)L_57)))/(float)(16.19f))))))), ((float)((float)(((float)((float)L_58)))/(float)(5.8f))), ((float)((float)(((float)((float)L_59)))/(float)(19.8f))), /*hidden argument*/NULL);
		int32_t L_61 = __this->get_current_bet_type_32();
		int32_t L_62 = V_2;
		Texture2D_t3840446185 * L_63 = Main_typeofbutton_m2032441888(__this, L_61, L_62, /*hidden argument*/NULL);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_60, L_63, /*hidden argument*/NULL);
		Color_t2555686324  L_64 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		GUISkin_t1244372282 * L_65 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_65);
		GUIStyle_t3956901511 * L_66 = GUISkin_get_label_m1693050720(L_65, /*hidden argument*/NULL);
		int32_t L_67 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		GUIStyle_set_fontSize_m1566850023(L_66, ((int32_t)((int32_t)L_67/(int32_t)((int32_t)30))), /*hidden argument*/NULL);
		int32_t L_68 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_69 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_70 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_71 = V_2;
		int32_t L_72 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_73 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_74 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Rect__ctor_m2614021312((&L_75), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_68)))/(float)(5.22f))), (float)((float)((float)(((float)((float)L_69)))/(float)(34.31f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_70)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_71))), (float)((float)((float)(((float)((float)L_72)))/(float)(16.3f))))))), ((float)((float)(((float)((float)L_73)))/(float)(6.88f))), ((float)((float)(((float)((float)L_74)))/(float)(17.55f))), /*hidden argument*/NULL);
		List_1_t3319525431 * L_76 = __this->get_horseracers_names_26();
		int32_t L_77 = V_2;
		NullCheck(L_76);
		String_t* L_78 = List_1_get_Item_m2840606808(L_76, L_77, /*hidden argument*/List_1_get_Item_m2840606808_RuntimeMethod_var);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_75, L_78, /*hidden argument*/NULL);
		int32_t L_79 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_80 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_81 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_82 = V_2;
		int32_t L_83 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_84 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_85 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Rect__ctor_m2614021312((&L_86), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_79)))/(float)(18.37f))), (float)((float)((float)(((float)((float)L_80)))/(float)(34.31f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_81)))/(float)(1.86f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_82))), (float)((float)((float)(((float)((float)L_83)))/(float)(16.3f))))))), ((float)((float)(((float)((float)L_84)))/(float)(7.82f))), ((float)((float)(((float)((float)L_85)))/(float)(17.73f))), /*hidden argument*/NULL);
		List_1_t1017553631 * L_87 = __this->get_horseracers_images_16();
		int32_t L_88 = V_2;
		NullCheck(L_87);
		Texture2D_t3840446185 * L_89 = List_1_get_Item_m3379783094(L_87, L_88, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_86, L_89, /*hidden argument*/NULL);
		goto IL_05a7;
	}

IL_0383:
	{
		int32_t L_90 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_91 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_92 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_93 = V_2;
		int32_t L_94 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_95 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_96 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_4), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_90)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_91)))/(float)(2.21f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_92)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_93, (int32_t)6))))), (float)((float)((float)(((float)((float)L_94)))/(float)(16.19f))))))), ((float)((float)(((float)((float)L_95)))/(float)(5.8f))), ((float)((float)(((float)((float)L_96)))/(float)(19.8f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_97 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_043a;
		}
	}
	{
		Event_t2956885303 * L_98 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_98);
		Vector2_t2156229523  L_99 = Event_get_mousePosition_m733809635(L_98, /*hidden argument*/NULL);
		bool L_100 = Rect_Contains_m3188543637((&V_4), L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_043a;
		}
	}
	{
		bool L_101 = __this->get_click_once_11();
		if (L_101)
		{
			goto IL_043a;
		}
	}
	{
		int32_t L_102 = __this->get_currentSel_29();
		if ((((int32_t)L_102) == ((int32_t)1)))
		{
			goto IL_043a;
		}
	}
	{
		int32_t L_103 = V_2;
		int32_t L_104 = L_103;
		RuntimeObject * L_105 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_104);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_106 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1945711025, L_105, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		int32_t L_107 = __this->get_current_bet_type_32();
		int32_t L_108 = V_2;
		Main_updatebet_m529302127(__this, L_107, L_108, /*hidden argument*/NULL);
		__this->set_click_once_11((bool)1);
	}

IL_043a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_109 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_044c;
		}
	}
	{
		__this->set_click_once_11((bool)0);
	}

IL_044c:
	{
		Color_t2555686324  L_110 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		GUISkin_t1244372282 * L_111 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_111);
		GUIStyle_t3956901511 * L_112 = GUISkin_get_label_m1693050720(L_111, /*hidden argument*/NULL);
		int32_t L_113 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_112);
		GUIStyle_set_fontSize_m1566850023(L_112, ((int32_t)((int32_t)L_113/(int32_t)((int32_t)30))), /*hidden argument*/NULL);
		int32_t L_114 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_115 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_116 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_117 = V_2;
		int32_t L_118 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_119 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_120 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_121;
		memset(&L_121, 0, sizeof(L_121));
		Rect__ctor_m2614021312((&L_121), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_114)))/(float)(5.22f))), (float)((float)((float)(((float)((float)L_115)))/(float)(2.21f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_116)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_117, (int32_t)6))))), (float)((float)((float)(((float)((float)L_118)))/(float)(16.3f))))))), ((float)((float)(((float)((float)L_119)))/(float)(6.88f))), ((float)((float)(((float)((float)L_120)))/(float)(17.55f))), /*hidden argument*/NULL);
		List_1_t3319525431 * L_122 = __this->get_horseracers_names_26();
		int32_t L_123 = V_2;
		NullCheck(L_122);
		String_t* L_124 = List_1_get_Item_m2840606808(L_122, L_123, /*hidden argument*/List_1_get_Item_m2840606808_RuntimeMethod_var);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_121, L_124, /*hidden argument*/NULL);
		int32_t L_125 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_126 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_127 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_128 = V_2;
		int32_t L_129 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_130 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_131 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_132;
		memset(&L_132, 0, sizeof(L_132));
		Rect__ctor_m2614021312((&L_132), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_125)))/(float)(18.37f))), (float)((float)((float)(((float)((float)L_126)))/(float)(2.21f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_127)))/(float)(1.86f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_128, (int32_t)6))))), (float)((float)((float)(((float)((float)L_129)))/(float)(16.3f))))))), ((float)((float)(((float)((float)L_130)))/(float)(7.82f))), ((float)((float)(((float)((float)L_131)))/(float)(17.73f))), /*hidden argument*/NULL);
		List_1_t1017553631 * L_133 = __this->get_horseracers_images_16();
		int32_t L_134 = V_2;
		NullCheck(L_133);
		Texture2D_t3840446185 * L_135 = List_1_get_Item_m3379783094(L_133, L_134, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_132, L_135, /*hidden argument*/NULL);
		Color_t2555686324  L_136 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_136, /*hidden argument*/NULL);
		int32_t L_137 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_138 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_139 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_140 = V_2;
		int32_t L_141 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_142 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_143 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Rect__ctor_m2614021312((&L_144), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_137)))/(float)(2.9f))), (float)((float)((float)(((float)((float)L_138)))/(float)(2.21f))))), ((float)il2cpp_codegen_add((float)((float)((float)(((float)((float)L_139)))/(float)(1.85f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_140, (int32_t)6))))), (float)((float)((float)(((float)((float)L_141)))/(float)(16.19f))))))), ((float)((float)(((float)((float)L_142)))/(float)(5.8f))), ((float)((float)(((float)((float)L_143)))/(float)(19.8f))), /*hidden argument*/NULL);
		int32_t L_145 = __this->get_current_bet_type_32();
		int32_t L_146 = V_2;
		Texture2D_t3840446185 * L_147 = Main_typeofbutton_m2032441888(__this, L_145, L_146, /*hidden argument*/NULL);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_144, L_147, /*hidden argument*/NULL);
	}

IL_05a7:
	{
		int32_t L_148 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_148, (int32_t)1));
	}

IL_05ab:
	{
		int32_t L_149 = V_2;
		List_1_t1017553631 * L_150 = __this->get_horseracers_images_16();
		NullCheck(L_150);
		int32_t L_151 = List_1_get_Count_m1676150876(L_150, /*hidden argument*/List_1_get_Count_m1676150876_RuntimeMethod_var);
		if ((((int32_t)L_149) < ((int32_t)L_151)))
		{
			goto IL_015b;
		}
	}
	{
		float L_152 = __this->get_shade_alpha_2();
		Color_t2555686324  L_153;
		memset(&L_153, 0, sizeof(L_153));
		Color__ctor_m2943235014((&L_153), (1.0f), (1.0f), (1.0f), L_152, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_color_m1028198571(NULL /*static, unused*/, L_153, /*hidden argument*/NULL);
		int32_t L_154 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_155 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_156;
		memset(&L_156, 0, sizeof(L_156));
		Rect__ctor_m2614021312((&L_156), (0.0f), (0.0f), (((float)((float)L_154))), (((float)((float)L_155))), /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_157 = __this->get_shade_14();
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_156, L_157, /*hidden argument*/NULL);
		Color_t2555686324  L_158;
		memset(&L_158, 0, sizeof(L_158));
		Color__ctor_m2943235014((&L_158), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		GUI_set_color_m1028198571(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		int32_t L_159 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_160 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_161 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_162 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_163;
		memset(&L_163, 0, sizeof(L_163));
		Rect__ctor_m2614021312((&L_163), ((float)((float)(((float)((float)L_159)))/(float)(1.25f))), ((float)((float)(((float)((float)L_160)))/(float)(1.107f))), ((float)((float)(((float)((float)L_161)))/(float)(4.8f))), ((float)((float)(((float)((float)L_162)))/(float)(9.36f))), /*hidden argument*/NULL);
		List_1_t1017553631 * L_164 = __this->get_chips_images_18();
		NullCheck(L_164);
		Texture2D_t3840446185 * L_165 = List_1_get_Item_m3379783094(L_164, 0, /*hidden argument*/List_1_get_Item_m3379783094_RuntimeMethod_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_163, L_165, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_166 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_166)
		{
			goto IL_06ed;
		}
	}
	{
		int32_t L_167 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_168 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_169 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_170 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_5), ((float)((float)(((float)((float)L_167)))/(float)(1.25f))), ((float)((float)(((float)((float)L_168)))/(float)(1.107f))), ((float)((float)(((float)((float)L_169)))/(float)(4.8f))), ((float)((float)(((float)((float)L_170)))/(float)(9.36f))), /*hidden argument*/NULL);
		Event_t2956885303 * L_171 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_171);
		Vector2_t2156229523  L_172 = Event_get_mousePosition_m733809635(L_171, /*hidden argument*/NULL);
		bool L_173 = Rect_Contains_m3188543637((&V_5), L_172, /*hidden argument*/NULL);
		if (!L_173)
		{
			goto IL_06ed;
		}
	}
	{
		bool L_174 = __this->get_click_once_11();
		if (L_174)
		{
			goto IL_06ed;
		}
	}
	{
		int32_t L_175 = __this->get_currentSel_29();
		if ((((int32_t)L_175) == ((int32_t)1)))
		{
			goto IL_06ed;
		}
	}
	{
		__this->set_shade_alpha_2((0.5f));
		__this->set_currentSel_29(1);
		__this->set_click_once_11((bool)1);
	}

IL_06ed:
	{
		int32_t L_176 = __this->get_currentSel_29();
		if ((!(((uint32_t)L_176) == ((uint32_t)1))))
		{
			goto IL_084c;
		}
	}
	{
		V_6 = 0;
		goto IL_0812;
	}

IL_0701:
	{
		int32_t L_177 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_178 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_179 = V_6;
		int32_t L_180 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_181 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_182 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((&V_7), ((float)((float)(((float)((float)L_177)))/(float)(1.25f))), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_178)))/(float)(1.107f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_179))), (float)((float)((float)(((float)((float)L_180)))/(float)(11.29f))))))), ((float)((float)(((float)((float)L_181)))/(float)(4.8f))), ((float)((float)(((float)((float)L_182)))/(float)(9.36f))), /*hidden argument*/NULL);
		int32_t L_183 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_184 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_185 = V_6;
		int32_t L_186 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_187 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_188 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_189;
		memset(&L_189, 0, sizeof(L_189));
		Rect__ctor_m2614021312((&L_189), ((float)((float)(((float)((float)L_183)))/(float)(1.25f))), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_184)))/(float)(1.107f))), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_185))), (float)((float)((float)(((float)((float)L_186)))/(float)(11.29f))))))), ((float)((float)(((float)((float)L_187)))/(float)(4.8f))), ((float)((float)(((float)((float)L_188)))/(float)(9.36f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_190 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle_t3956901511 * L_191 = GUIStyle_get_none_m1545577352(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_192 = GUI_Button_m2223708732(NULL /*static, unused*/, L_189, L_190, L_191, /*hidden argument*/NULL);
		if (!L_192)
		{
			goto IL_07b8;
		}
	}
	{
		bool L_193 = __this->get_chip_notpressed_12();
		if (!L_193)
		{
			goto IL_07b8;
		}
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral2474120682, /*hidden argument*/NULL);
	}

IL_07b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_194 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_194)
		{
			goto IL_080c;
		}
	}
	{
		Event_t2956885303 * L_195 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_195);
		Vector2_t2156229523  L_196 = Event_get_mousePosition_m733809635(L_195, /*hidden argument*/NULL);
		bool L_197 = Rect_Contains_m3188543637((&V_7), L_196, /*hidden argument*/NULL);
		if (L_197)
		{
			goto IL_080c;
		}
	}
	{
		bool L_198 = __this->get_click_once_11();
		if (L_198)
		{
			goto IL_080c;
		}
	}
	{
		__this->set_click_once_11((bool)1);
		__this->set_shade_alpha_2((0.0f));
		__this->set_currentSel_29(0);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral326071421, /*hidden argument*/NULL);
		goto IL_080c;
	}

IL_080c:
	{
		int32_t L_199 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_199, (int32_t)1));
	}

IL_0812:
	{
		int32_t L_200 = V_6;
		List_1_t1017553631 * L_201 = __this->get_chips_images_18();
		NullCheck(L_201);
		int32_t L_202 = List_1_get_Count_m1676150876(L_201, /*hidden argument*/List_1_get_Count_m1676150876_RuntimeMethod_var);
		if ((((int32_t)L_200) < ((int32_t)L_202)))
		{
			goto IL_0701;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_203 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_203)
		{
			goto IL_084c;
		}
	}
	{
		int32_t L_204 = __this->get_currentSel_29();
		if ((!(((uint32_t)L_204) == ((uint32_t)1))))
		{
			goto IL_084c;
		}
	}
	{
		__this->set_chip_notpressed_12((bool)1);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral4122535824, /*hidden argument*/NULL);
	}

IL_084c:
	{
		return;
	}
}
// System.Void Main::MouseEvents()
extern "C"  void Main_MouseEvents_m2425756171 (Main_t2227614074 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Main::.cctor()
extern "C"  void Main__cctor_m2675706324 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main__cctor_m2675706324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Main_t2227614074_StaticFields*)il2cpp_codegen_static_fields_for(Main_t2227614074_il2cpp_TypeInfo_var))->set_gui_depth_4(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Main/<Timer>c__Iterator0::.ctor()
extern "C"  void U3CTimerU3Ec__Iterator0__ctor_m1791513085 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Main/<Timer>c__Iterator0::MoveNext()
extern "C"  bool U3CTimerU3Ec__Iterator0_MoveNext_m3832737386 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerU3Ec__Iterator0_MoveNext_m3832737386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0099;
			}
		}
	}
	{
		goto IL_00bf;
	}

IL_0021:
	{
		float L_2 = __this->get_t_0();
		__this->set_U3CrateU3E__0_1(((float)((float)(1.0f)/(float)L_2)));
		__this->set_U3CindexU3E__0_2((0.0f));
		__this->set_U3CindexU3E__0_2((0.0f));
		goto IL_0099;
	}

IL_004e:
	{
		float L_3 = __this->get_U3CindexU3E__0_2();
		float L_4 = __this->get_U3CrateU3E__0_1();
		float L_5 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CindexU3E__0_2(((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)))));
		float L_6 = __this->get_t_0();
		float L_7 = __this->get_U3CindexU3E__0_2();
		float L_8 = ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7));
		RuntimeObject * L_9 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_8);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_10 = __this->get_U24disposing_4();
		if (L_10)
		{
			goto IL_0094;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0094:
	{
		goto IL_00c1;
	}

IL_0099:
	{
		float L_11 = __this->get_U3CindexU3E__0_2();
		if ((((double)(((double)((double)L_11)))) < ((double)(1.0))))
		{
			goto IL_004e;
		}
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral2791739664, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
}
// System.Object Main/<Timer>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CTimerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3725364125 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Main/<Timer>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CTimerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2380589530 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void Main/<Timer>c__Iterator0::Dispose()
extern "C"  void U3CTimerU3Ec__Iterator0_Dispose_m2583745617 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Main/<Timer>c__Iterator0::Reset()
extern "C"  void U3CTimerU3Ec__Iterator0_Reset_m2192134946 (U3CTimerU3Ec__Iterator0_t685532230 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerU3Ec__Iterator0_Reset_m2192134946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
