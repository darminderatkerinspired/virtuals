﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiLabels : MonoBehaviour {
    public static int GUIdepth = 0;
    public float change1, change2, change3, change4, change5;
    public int change6;
    public Main mainscript;

    public List<string> betplacenames = new List<string>();
	// Use this for initialization
	void Start () {
        Main.gui_depth = 1;
	}
    public static void Gui_Change(){
        Main.chip_gui_depth = -1;
        Main.gui_depth = -1;
    }
    string bet_Value_topdisplay(int betype, int betnum)
    {
        if (mainscript.current_bets_master[betype][betnum] < 1f)
        {
            return mainscript.bet_odds_master[betype][betnum] .ToString("C");
        }
        else
        {
            return mainscript.current_bets_master[betype][betnum].ToString("C");
        }
    }
    void OnGUI()
    {
        
        GUI.depth = GUIdepth;
        for (int i = 0; i < mainscript.horseracers_images.Count; i++)
        {
            GUI.contentColor = Color.black;
            GUI.skin.label.fontSize = Screen.width / 30;
            if (i < 6)
            {
                GUI.Label(new Rect(Screen.width / 2.9f - (Screen.width / 34.31f) + Screen.width / 34.67f, Screen.height / 1.85f + (i * (Screen.height / 16.19f)) + Screen.height / 123.6f, Screen.width / 6.32f, Screen.height / 1.89f), bet_Value_topdisplay(mainscript.current_bet_type,i));
            }
            else
            {
                GUI.Label(new Rect(Screen.width / 2.9f - (Screen.width / 34.31f) + Screen.width / 34.67f + Screen.width / 2.06f, Screen.height / 1.85f + ((i - 6) * (Screen.height / 16.19f)) + Screen.height / 123.6f, Screen.width / 6.32f, Screen.height / 1.89f), bet_Value_topdisplay(mainscript.current_bet_type, i));     
            }

        }
        for (int i = 0; i < 3; i++)
        {
            GUI.Label(new Rect(Screen.width / 6f + (i * (Screen.width / 4.52f)) + Screen.width / 23f, Screen.height / 1.096f + Screen.height / 35.43f, Screen.width / 5f, Screen.height / 11.59f), betplacenames[i]);
        }
        if (mainscript.currentSel == Main.Selection.ChipsPressed)
        {
            for (int i = 0; i < mainscript.chips_images.Count; i++)
            {
                GUI.DrawTexture(new Rect(Screen.width / 1.25f, Screen.height / 1.107f - (i * (Screen.height / 11.29f)), Screen.width / 4.8f, Screen.height / 9.36f), mainscript.chips_images[i]);
            }
        }
        //GUI.Label(new Rect(Screen.width / 1.25f + Screen.width / 12f, Screen.height / 1.107f + Screen.height / 27.76f, Screen.width / 8.78f, Screen.height / 5.18f), "$1");
    }
}
