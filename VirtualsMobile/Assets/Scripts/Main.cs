﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {
    float shade_alpha = 0.0f;
    public static int chip_gui_depth = 0;
    public static int gui_depth = 1;
	public float change1, change2, change3, change4,change5;
    public int change6 ;
    bool click_once = false;
    bool chip_notpressed = false;
  
	public Texture2D Template;
    public Texture2D shade;

	public List<GameObject> sceneobjects = new List<GameObject> ();
	public List<Texture2D> horseracers_images = new List<Texture2D>();
    public List<Texture2D> bet_btns = new List<Texture2D>();
    public List<Texture2D> chips_images = new List<Texture2D>();

    public List<float> horse_bets_win = new List<float>();
    public List<float> horse_bets_place = new List<float>();
    public List<float> horse_bets_show = new List<float>();

    public List<int> horses_finish = new List<int>();

    public List<float> current_bets_win = new List<float>();
    public List<float> current_bets_place = new List<float>();
    public List<float> current_bets_show = new List<float>();
	public List<string> horseracers_names =  new List<string>();

    public List<List<float>> bet_odds_master = new List<List<float>>();
    public List<List<float>> current_bets_master = new List<List<float>>();

    public enum Selection { None, ChipsPressed, ChipsToBePressed };
    public  Selection currentSel;
    float stake = 1f;
    float visual_timer = 0;
    public int current_bet_type = 0;
	// Use this for initialization
	void Start () {
      currentSel = Selection.None;

      bet_odds_master.Add(horse_bets_win);
      bet_odds_master.Add(horse_bets_place);
      bet_odds_master.Add(horse_bets_show);

      current_bets_master.Add(current_bets_win);
      current_bets_master.Add(current_bets_place);
      current_bets_master.Add(current_bets_show);

      reset_bets();
      GuiLabels.GUIdepth = 0;
      randomwinners();
      //StartCoroutine(Timer(5f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Calculate horse winner 
    void randomwinners()
    {
        horses_finish.Clear();
        List<int> temp_horse_list = new List<int>();
        for (int i = 0; i < horse_bets_win.Count; i++)
        {
            temp_horse_list.Add(i);
        }
        for (int i = 0; i < horse_bets_win.Count; i++)
        {
            int horsesleft = temp_horse_list.Count;
            int random = Random.Range(0, horsesleft);
            horses_finish.Add(temp_horse_list[random]);
            temp_horse_list.RemoveAt(random);
        }

    }
    void Calculate_wins()
    {
        
        int winner = horses_finish[0];
        int second = horses_finish[1];
        int third = horses_finish[2];
        float winamount = 0;
        for (int i = 0; i < current_bets_master.Count; i++)
        {
            for (int j = 0; j < current_bets_win.Count; j++)
            {
                if (i == 0) //Win Bet
                {
                    
                    if (current_bets_master[i][winner] > 0f && j == winner)
                    {
                        winamount += current_bets_master[i][winner] * bet_odds_master[i][winner];
                        print("Win: " + winamount + " Odds: " + bet_odds_master[i][winner] + " Stake: " + current_bets_master[i][winner]);
                    }
                }
                if (i == 1) //Place Bet
                {
                    if (current_bets_master[i][winner] > 0f && j == winner)
                    {
                        winamount += current_bets_master[i][winner] * bet_odds_master[i][winner];
                        print("Win: " + winamount + " Odds: " + bet_odds_master[i][winner] + " Stake: " + current_bets_master[i][winner] + " Horse: " + winner);
                    }
                    if (current_bets_master[i][second] > 0f && j == second)
                    {
                        winamount += current_bets_master[i][second] * bet_odds_master[i][second];
                        print("Win: " + winamount + " Odds: " + bet_odds_master[i][second] + " Stake: " + current_bets_master[i][second] + " Horse: " + second);
                    }
                }
            }

        }
    }
    IEnumerator Timer(float t)
    {

        float rate = 1.0f / t;
        float index = 0.0f;

        index = 0.0f;
       

        while (index < 1.0)
        {

            index += rate * Time.deltaTime;
            print(t*index);
            yield return null;
        }
        print("done");
    }
    //Reset all current bets to 0 
    void reset_bets()
    {

        for (int i = 0; i < bet_odds_master.Count; i++)
        {
            current_bets_master[i].Clear();
            for (int j = 0; j < horse_bets_win.Count; j++)
            {
                current_bets_master[i].Add(0f);
            }
              
        }
    }
    //Update current bet 
    void updatebet(int bet_type, int bet_ID)
    {
        current_bets_master[bet_type][bet_ID] += stake; 
    }
    Texture2D typeofbutton(int betype, int betnum)
    {
        if (current_bets_master[betype][betnum] < 1f)
        {
            return bet_btns[0];
        }
        else
        {
            return bet_btns[2];
        }
    }
    void OnGUI()
    {


        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Template);
        if (GUI.Button(new Rect(0, 0, 100, 100), "BET"))
        {
            Calculate_wins();
        }
        for (int i = 0; i < 3; i++)
        {
            GUI.DrawTexture(new Rect(Screen.width / 6f + (i * (Screen.width / 4.52f)), Screen.height / 1.096f, Screen.width / 5f, Screen.height / 11.59f), bet_btns[1]);
            Rect r = new Rect(Screen.width / 6f + (i * (Screen.width / 4.52f)), Screen.height / 1.096f, Screen.width / 5f, Screen.height / 11.59f);
            if (Input.GetMouseButtonDown(0) && currentSel != Selection.ChipsPressed)
            {

                if (r.Contains(Event.current.mousePosition) && !click_once)
                {
                    current_bet_type = i;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                click_once = false;
            }
        }
            for (int i = 0; i < horseracers_images.Count; i++)
            {
                if (i < 6)
                {
                    Rect r = new Rect(Screen.width / 2.9f - (Screen.width / 34.31f), Screen.height / 1.85f + (i * (Screen.height / 16.19f)), Screen.width / 5.8f, Screen.height / 19.8f);
                    if (Input.GetMouseButtonDown(0) && currentSel != Selection.ChipsPressed)
                    {

                        if (r.Contains(Event.current.mousePosition) && !click_once)
                        {
                            print("clicked" + i);
                            updatebet(current_bet_type, i);
                            click_once = true;
                        }
                    }
                    if (Input.GetMouseButtonUp(0))
                    {
                        click_once = false;
                    }


                    GUI.contentColor = Color.white;
                    GUI.DrawTexture(new Rect(Screen.width / 2.9f - (Screen.width / 34.31f), Screen.height / 1.85f + (i * (Screen.height / 16.19f)), Screen.width / 5.8f, Screen.height / 19.8f), typeofbutton(current_bet_type,i));

                    GUI.contentColor = Color.black;

                    GUI.skin.label.fontSize = Screen.width / 30;
                    GUI.Label(new Rect(Screen.width / 5.22f - (Screen.width / 34.31f), Screen.height / 1.85f + (i * (Screen.height / 16.3f)), Screen.width / 6.88f, Screen.height / 17.55f), horseracers_names[i]);
                    GUI.DrawTexture(new Rect(Screen.width / 18.37f - (Screen.width / 34.31f), Screen.height / 1.86f + (i * (Screen.height / 16.3f)), Screen.width / 7.82f, Screen.height / 17.73f), horseracers_images[i]);

                }
                else
                {
                    Rect r = new Rect(Screen.width / 2.9f + (Screen.width / 2.21f), Screen.height / 1.85f + ((i - 6) * (Screen.height / 16.19f)), Screen.width / 5.8f, Screen.height / 19.8f);
                    if (Input.GetMouseButtonDown(0))
                    {

                        if (r.Contains(Event.current.mousePosition) && !click_once && currentSel != Selection.ChipsPressed)
                        {
                            print("clicked" + i);
                            updatebet(current_bet_type, i);
                            click_once = true;
                        }
                    }
                    if (Input.GetMouseButtonUp(0))
                    {
                        click_once = false;
                    }
                    GUI.contentColor = Color.black;
                    GUI.skin.label.fontSize = Screen.width / 30;
                    GUI.Label(new Rect(Screen.width / 5.22f + (Screen.width / 2.21f), Screen.height / 1.85f + ((i - 6) * (Screen.height / 16.3f)), Screen.width / 6.88f, Screen.height / 17.55f), horseracers_names[i]);
                    GUI.DrawTexture(new Rect(Screen.width / 18.37f + (Screen.width / 2.21f), Screen.height / 1.86f + ((i - 6) * (Screen.height / 16.3f)), Screen.width / 7.82f, Screen.height / 17.73f), horseracers_images[i]);
                    GUI.contentColor = Color.white;
                    GUI.DrawTexture(new Rect(Screen.width / 2.9f + (Screen.width / 2.21f), Screen.height / 1.85f + ((i - 6) * (Screen.height / 16.19f)), Screen.width / 5.8f, Screen.height / 19.8f), typeofbutton(current_bet_type, i));


                }
            }

        GUI.color = new Color(1, 1, 1, shade_alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shade);
        GUI.color = new Color(1, 1, 1, 1f);
        GUI.DrawTexture(new Rect(Screen.width / 1.25f, Screen.height / 1.107f, Screen.width / 4.8f, Screen.height / 9.36f), chips_images[0]);
        if (Input.GetMouseButtonDown(0))
        {
            Rect r = new Rect(Screen.width / 1.25f, Screen.height / 1.107f, Screen.width / 4.8f, Screen.height / 9.36f);
            if (r.Contains(Event.current.mousePosition) && !click_once && currentSel != Selection.ChipsPressed)
            {
                shade_alpha = 0.5f;
                currentSel = Selection.ChipsPressed;
                click_once = true;
            }
        }
       
        if (currentSel == Selection.ChipsPressed)
        {
            
                for (int i = 0; i < chips_images.Count; i++)
                {
                    Rect r2 = new Rect(Screen.width / 1.25f, Screen.height / 1.107f - (i * (Screen.height / 11.29f)), Screen.width / 4.8f, Screen.height / 9.36f);
                    if (GUI.Button(new Rect(Screen.width / 1.25f, Screen.height / 1.107f - (i * (Screen.height / 11.29f)), Screen.width / 4.8f, Screen.height / 9.36f), "", GUIStyle.none) && chip_notpressed == true)
                    {
                        print("hitchip");
                       // currentSel = Selection.None;
                    }

                    if (Input.GetMouseButtonDown(0))
                    {
                        
                       
                        if (r2.Contains(Event.current.mousePosition) == false && !click_once )
                        {
                            click_once = true;
                            shade_alpha = 0.0f;
                            currentSel = Selection.None;
                            print("removeselection");
                        }
                       
                        else
                        {
                           
                            //shade_alpha = 0.0f;
                            //currentSel = Selection.None;
                            // click_once = true;
                        }
                    }
                  

                }
                if (Input.GetMouseButtonUp(0) && currentSel == Selection.ChipsPressed)
                {
                    chip_notpressed = true;
                    print("realsed");
                }
            
        }
        
    }
    void MouseEvents()
    {

    }
}
